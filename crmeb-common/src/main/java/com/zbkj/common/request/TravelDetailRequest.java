package com.zbkj.common.request;

import lombok.Data;

@Data
public class TravelDetailRequest extends CommonSearchRequest{
    private Integer travelId;
}
