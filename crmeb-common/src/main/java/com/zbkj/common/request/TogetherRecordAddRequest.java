package com.zbkj.common.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.zbkj.common.model.product.Product;
import com.zbkj.common.model.user.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class TogetherRecordAddRequest {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer uid;
    private Integer  togetherId;
    private String  title;
    private Integer  type;
    private String  img;
    private String  targetImg;
    private String  targetName;
    private Integer  targetId;


    @TableField(exist = false)
    private User user;

    @TableField(exist = false)
    private List<Product> product;

    private String  content;


}
