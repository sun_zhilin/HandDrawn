package com.zbkj.common.request;

import lombok.Data;

@Data
public class NovelHistoryRequest {

    private Integer bookId;

    private float progress;

    private Integer chapterIndex;
}
