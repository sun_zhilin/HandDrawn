package com.zbkj.common.request;

import lombok.Data;

@Data
public class TogetherAttachRequest {

    private Integer type;

    private Integer togetherId;

    private String fileUrl;

    private String fileName;

    private String fileExt;

    private String fileType;

    private Integer uid;
}
