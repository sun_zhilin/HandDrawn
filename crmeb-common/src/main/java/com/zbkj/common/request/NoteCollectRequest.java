package com.zbkj.common.request;

import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;

@Data
public class NoteCollectRequest implements Serializable {


    private static final long serialVersionUID = 1L;

    @NotNull(message = "请选择笔记")
    @Min(value = 1, message = "请选择笔记")
    private Integer noteId;
}
