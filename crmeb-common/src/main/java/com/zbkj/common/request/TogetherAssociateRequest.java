package com.zbkj.common.request;

import lombok.Data;

@Data
public class TogetherAssociateRequest {
    private Integer togetherId;
    private Integer targetId;
    private Integer type;
}
