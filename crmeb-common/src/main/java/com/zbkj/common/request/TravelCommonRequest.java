package com.zbkj.common.request;

import lombok.Data;

@Data
public class TravelCommonRequest {
    private String name;
    private String phone;
}
