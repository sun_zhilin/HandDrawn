package com.zbkj.common.request;

import lombok.Data;

@Data
public class CustomAttachUploadRequest {

    private Integer customId;
    private Integer type;

    private String fileUrl;

    private String fileName;

    private String fileType;
}
