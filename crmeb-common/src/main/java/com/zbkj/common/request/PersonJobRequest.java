package com.zbkj.common.request;

import lombok.Data;

@Data
public class PersonJobRequest extends PageParamRequest{
    private Integer id;
}
