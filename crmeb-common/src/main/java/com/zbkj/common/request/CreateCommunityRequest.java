package com.zbkj.common.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CreateCommunityRequest", description="创建订单请求对象")
public class CreateCommunityRequest {
    private static final long serialVersionUID = -6133994384185333872L;

    @ApiModelProperty(value = "预下单订单号", required = true)
    @NotBlank(message = "预下单订单号不能为空")
    private String orderNo;

    @ApiModelProperty(value = "支付方式", required = true)
    @NotBlank(message = "支付方式")
    private String payChannel;

    @ApiModelProperty(value = "支付方式", required = true)
    @NotBlank(message = "支付方式")
    private String payType;

    @ApiModelProperty(value = "场景", required = true)
    @NotBlank(message = "场景")
    private String scene;
}
