package com.zbkj.common.request;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class NovelChapterUpdateRequest {
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    @ApiModelProperty(value = "章节id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    /**
     * 章节名
     */
    private String chapterName;
    /**
     * 章节数
     */
    private Integer chapterNum;
    /**
     * 小说id
     */
    private Integer novelId;
    /**
     * 卷id
     */
    private Integer rollId;

    private String content;

}
