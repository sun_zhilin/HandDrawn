package com.zbkj.common.request;

import lombok.Data;

@Data
public class TravelShopRequest extends PageParamRequest{
    private Integer travelId;
}
