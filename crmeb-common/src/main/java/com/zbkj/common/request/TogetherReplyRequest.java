package com.zbkj.common.request;


import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "TogetherReplyRequest对象", description = "评论添加请求对象")
public class TogetherReplyRequest {

    @ApiModelProperty(value = "笔记ID")
    @NotNull(message = "笔记ID不能为空")
    private Integer togetherId;

    @ApiModelProperty(value = "评论ID，一级评论传0")
    @NotNull(message = "评论ID不能为空")
    private Integer replyId;

    @ApiModelProperty(value = "评论/回复内容")
    @Length(max = 200, message = "内容不能超过200个字符")
    private String content;

    @ApiModelProperty(value = "用户ID，移动端不传值")
    private Integer userId;
}
