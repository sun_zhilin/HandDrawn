package com.zbkj.common.request;

import lombok.Data;

@Data
public class CustomUpdateRequest {
    private String orderNo;
    private Integer customId;
}
