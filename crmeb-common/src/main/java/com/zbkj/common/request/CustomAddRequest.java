package com.zbkj.common.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import software.amazon.ion.Decimal;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Date;

@Data
public class CustomAddRequest {
    private String name;
    private BigDecimal firstMoney;
    private BigDecimal lastMoney;
    private String firstTime;
    private String lastTime;
    private Integer productId;
    private Integer feedbackTime;
    @ApiModelProperty(value = "定制提供方id")
    private Integer proId;
}
