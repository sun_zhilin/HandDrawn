package com.zbkj.common.request;

import com.zbkj.common.annotation.StringContains;
import com.zbkj.common.model.communityOrder.CommunityOrder;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "CommunityPreOrderRequest", description = "预下单请求对象")
public class CommunityPreOrderRequest {

    @ApiModelProperty(value = "预下单类型（“shoppingCart”：购物车下单，“buyNow”：立即购买，”again“： 再次购买，”video“: 视频号商品下单，”seckill“： 秒杀下单）", required = true)
    @NotBlank(message = "预下单类型不能为空")
    @StringContains(limitValues = {"shoppingCart", "buyNow", "again", "video", "seckill"}, message = "未知的预下单类型")
    private String preOrderType;



    private CommunityOrder communityOrder;
}
