package com.zbkj.common.request;

import lombok.Data;

@Data
public class CustomPreorderRequest {
    private Integer customId;
    private Integer type;

}
