package com.zbkj.common.request;

import lombok.Data;

@Data
public class CustomAttachUpload {

    private CustomAttachUploadRequest param;
}
