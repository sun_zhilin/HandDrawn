package com.zbkj.common.request;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotBlank;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "NoteTopicRequest对象", description = "笔记专题")
public class NoteTopicRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "id,修改时必传")
    private Integer id;

    @ApiModelProperty(value = "标签名称")
    @NotBlank(message = "请填写标签名称")
    @Length(max = 50, message = "标签名称不能超过50个字符")
    private String topicName;

    private Integer isFree;

    private BigDecimal price;

    private Date createTime;
}
