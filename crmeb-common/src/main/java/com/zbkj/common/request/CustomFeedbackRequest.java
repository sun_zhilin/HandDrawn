package com.zbkj.common.request;

import lombok.Data;

@Data
public class CustomFeedbackRequest {
    private Integer customId;
    private String content;
}
