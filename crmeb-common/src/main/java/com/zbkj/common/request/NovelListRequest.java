package com.zbkj.common.request;

import lombok.Data;

@Data
public class NovelListRequest extends PageParamRequest{
    private Integer cid;
}
