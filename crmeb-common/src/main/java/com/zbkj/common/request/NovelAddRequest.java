package com.zbkj.common.request;

import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

import java.util.Date;

@Data
public class NovelAddRequest {
    @TableId
    private Integer id;
    /**
     * 书名
     */
    private String articlename;
    /**
     * 作者id
     */
    private Integer uId;
    /**
     * 简介
     */
    private String intro;
    /**
     * 封面路径
     */
    private String urlImg;
    /**
     * 分类id
     */
    private Integer catId;
    /**
     * 编辑id
     */
    private Integer editorId;
    /**
     * 字数
     */
    private Integer length=0;
    /**
     * 收藏
     */
    private Integer collect=0;
    /**
     * 连载状态0连载1完结
     */
    private Integer status=0;
    /**
     * 有编辑权限的用户id
     */
    private String authorityIds;
    /**
     *
     */
    private Integer isShow=1;
    private Integer togetherId;
    /**
     *
     */
    private Integer isDel=0;
    /**
     *
     */
    private Date createTime;
    /**
     *
     */
    private Date updateTime;

    private Integer totalChapter=0;
}
