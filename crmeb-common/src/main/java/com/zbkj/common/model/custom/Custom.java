package com.zbkj.common.model.custom;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import software.amazon.ion.Decimal;

import java.math.BigDecimal;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_custom")
@ApiModel(value = "custom对象", description = "定制表")
public class Custom {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "定制id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String name;
    private Integer recId;
    private Integer prdId;
    private Integer status;
    private String preOrderId;
    private String lastOrderId;
    private Boolean isShow;
    private Integer feedback_time;
    private Integer productId;

    private BigDecimal firstMoney;
    private BigDecimal lastMoney;

    @ApiModelProperty(value = "是否删除")
    private Boolean isDel;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;
    private Date finallTime;
    private Date firstTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    private Date nextFeedTime;
}
