package com.zbkj.common.model.notice;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


import java.io.Serializable;
/**
 * 【请填写功能名称】对象 notice_type
 *
 * @author my
 * @date 2024-04-07
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("notice_type")
@ApiModel(value="NoticeType对象", description="通知类型表")
public class NoticeType implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 通知类型id */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /** 通知标题 */
    @ApiModelProperty(value = "通知标题")
    private String title;

    /** 通知内容 */
    @ApiModelProperty(value = "通知内容")
    private String content;

}
