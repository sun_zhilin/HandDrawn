package com.zbkj.common.model.novel;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

	import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * 小说章节表 Entity 实体类
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * @author: 大粽子
 * +----------------------------------------------------------------------
 * @date Mon Apr 22 15:31:27 CST 2024
 * +----------------------------------------------------------------------
 * @email stivepeim@outlook.com
 * +----------------------------------------------------------------------
 */
@Data
@TableName("tb_novel_roll_chapter")
public class NovelRollChapterEntity implements Serializable {
	private static final long serialVersionUID = 1L;

			/**
			 *
			 */
			@ApiModelProperty(value = "章节id")
			@TableId(value = "id", type = IdType.AUTO)
			private Integer id;
			/**
			 *
			 */
			private Integer isShow;
			/**
			 *
			 */
			private Integer isDel;
			/**
			 * c
			 */
			private Date createTime;
			/**
			 *
			 */
			private Date updateTime;
			/**
			 * 章节名
			 */
			private String chapterName;
			/**
			 * 章节数
			 */
			private Integer chapterNum;
			/**
			 * 小说id
			 */
			private Integer novelId;
			private Integer isOpen;
			/**
			 * 卷id
			 */
			private Integer rollId;

			private String content;

}
