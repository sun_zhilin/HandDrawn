package com.zbkj.common.model.together;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zbkj.common.model.Appeal;
import com.zbkj.common.model.user.User;
import com.zbkj.common.model.user.UserTag;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_together_job")
@ApiModel(value = "共创", description = "共创")
public class TogetherJob implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer uid;
    private Integer togetherId;
    private Integer jobId;
    private Integer togetherModel;
    private Integer isPay;
    private String requirement;
    private String fileUrl;
    private String remark;
    @TableField(exist = false)
    private String userName;
    @TableField(exist = false)
    private String userImg;

    @TableField(exist = false)
    private List<UserTag> jobs;
    @TableField(exist = false)
    private String jobName;

    @TableField(exist = false)
    private Long groupId;

    @TableField(exist = false)
    private Integer isProject;

    @TableField(exist = false)
    private Integer isTogether;

    private BigDecimal dividendProportion;
    private BigDecimal price;

    private BigDecimal  sharePrice;



    @ApiModelProperty(value = "0: 未加入 1: 已申请  2: 未同意  3:已加入 4:已拒绝")
    private Integer status;

    @ApiModelProperty(value = "是否删除 状态（0：否，1：是）")
    private Boolean isDel;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @TableField(exist = false)
    private User user;

    @TableField(exist = false)
    private UserTogether userTogether;

    @TableField(exist = false)
    private List<User> examUsers;

    @TableField(exist = false)
    private Appeal appeal;



}
