package com.zbkj.common.model.together;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@Accessors(chain = true)
@TableName("tb_together_reply_like")
@ApiModel(value = "TogetherReplyLike对象", description = "笔记评论点赞表")
public class TogetherReplyLike {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @ApiModelProperty("共创ID")
    @TableField("together_id")
    private Integer togetherId;

    @ApiModelProperty("评论ID")
    @TableField("reply_id")
    private Integer replyId;

    @ApiModelProperty("用户ID")
    @TableField("uid")
    private Integer uid;

    @ApiModelProperty("创建时间")
    @TableField("create_time")
    private Date createTime;
}
