package com.zbkj.common.model.communityOrder;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zbkj.common.model.community.CommunityNotes;
import com.zbkj.common.model.user.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("eb_community_order")
@ApiModel(value = "社区订单(临时订单,笔记订单，专题订单)", description = "社区订单(临时订单,笔记订单，专题订单)")
public class CommunityOrder implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer uid;
    private Integer userId;

    private String userName;

    private String userImg;

    private String orderSn;

    private Integer targetId;

    private Integer targetType;

    private BigDecimal totalPrice;

    private Integer payMethodType;

    private BigDecimal payPrice;

    private Integer status;

    @ApiModelProperty(value = "是否删除 状态（0：否，1：是）")
    private Boolean isDel;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @TableField(exist = false)
    private User user;

    @TableField(exist = false)
    private CommunityNotes notes;

    @TableField(exist = false)
    private NoteTopic noteTopic;
}
