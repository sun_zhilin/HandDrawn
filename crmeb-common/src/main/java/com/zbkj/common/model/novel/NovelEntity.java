package com.zbkj.common.model.novel;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

	import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.Data;

/**
 * 小说对象表 Entity 实体类
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * @author: 大粽子
 * +----------------------------------------------------------------------
 * @date Mon Apr 22 15:31:27 CST 2024
 * +----------------------------------------------------------------------
 * @email stivepeim@outlook.com
 * +----------------------------------------------------------------------
 */
@Data
@TableName("tb_novel")
public class NovelEntity implements Serializable {
	private static final long serialVersionUID = 1L;


			@ApiModelProperty(value = "小说id")
			@TableId(value = "id", type = IdType.AUTO)
			private Integer id;
			/**
			 * 书名
			 */
			private String articlename;
			/**
			 * 作者id
			 */
			private Integer uId;
			/**
			 * 简介
			 */
			private String intro;
			/**
			 * 封面路径
			 */
			private String urlImg;
			/**
			 * 分类id
			 */
			private Integer catId;
			/**
			 * 编辑id
			 */
			private Integer editorId;
			/**
			 * 字数
			 */
			private Integer length;
			/**
			 * 收藏
			 */
			private Integer collect;
			/**
			 * 连载状态0连载1完结
			 */
			private Integer status;
			/**
			 * 有编辑权限的用户id
			 */
			private Integer authorityIds;
			/**
			 *
			 */
			private Integer isShow;
			/**
			 *
			 */
			private Integer isDel;
			/**
			 *
			 */
			private Date createTime;
			/**
			 *
			 */
			private Date updateTime;

			private Integer totalChapter;

}
