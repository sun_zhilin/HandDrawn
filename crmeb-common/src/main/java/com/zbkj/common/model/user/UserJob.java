package com.zbkj.common.model.user;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_user_job")
@ApiModel(value="tb_user_job", description="用户job关联表")
public class UserJob {
    private static final long serialVersionUID=1L;

    private Integer id;
    private Integer uid;
    private Integer jobId;
    @ApiModelProperty(value = "添加时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "是否删除,0=未删除,1=删除")
    private Boolean isDel;


}
