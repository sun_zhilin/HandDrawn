package com.zbkj.common.model.together;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_user_together_attach")
@ApiModel(value = "TogetherAttach对象", description = "定制附件表")
public class TogetherAttach {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "附件id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer togetherId;
    private Integer type;

    private String fileUrl;

    private Integer uid;

    private String fileName;

    private String fileExt;

    private Boolean isShow;
    @ApiModelProperty(value = "是否删除")
    private Boolean isDel;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;
}

