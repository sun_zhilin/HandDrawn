package com.zbkj.common.model.together;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zbkj.common.model.user.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_user_together_demand")
@ApiModel(value = "共创任务", description = "共创任务")
public class UserTogetherDemand implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer uid;
    private Integer togetherId;
    private Integer userJobId;

    @TableField(exist = false)
    private String jobName;

    private String fileUrls;

    @TableField(exist = false)
    private String userName;
    @TableField(exist = false)
    private String userImg;

    private String title;
    private String content;

    @ApiModelProperty(value = "0：未开始 1:进行中 2 ：已完成")
    private Integer status;

    @ApiModelProperty(value = "是否删除 状态（0：否，1：是）")
    private Boolean isDel;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @TableField(exist = false)
    private User user;

    @TableField(exist = false)
    private UserTogether userTogether;


}
