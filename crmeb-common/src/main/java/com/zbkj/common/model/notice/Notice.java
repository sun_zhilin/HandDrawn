package com.zbkj.common.model.notice;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.apache.commons.lang3.builder.ToStringStyle;


import java.io.Serializable;
import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("notice")
@ApiModel(value="Notice对象", description="通知表")
public class Notice implements Serializable
{
    private static final long serialVersionUID = 1L;

    /** 通知id */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /** 商品类型通知，0则为自定义通知 */
    @ApiModelProperty(value = "typeId")
    private Long typeId;

    /** 用户id */
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /** 订单号 */
    @ApiModelProperty(value = "订单号")
    private String orderNo;

    /** 自定义内容 */
    @ApiModelProperty(value = "自定义内容")
    private String content;

    @ApiModelProperty(value = "自定义标题")
    private String title;

    @ApiModelProperty(value = "通知时间")
    private Date createTime;
}
