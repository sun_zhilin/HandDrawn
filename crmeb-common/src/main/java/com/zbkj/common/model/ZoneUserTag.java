package com.zbkj.common.model;

import com.baomidou.mybatisplus.annotation.TableName;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.util.Date;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_zone_user_tag")
@ApiModel(value = "ZoneUserTag对象", description = "角色板块关联表")
public class ZoneUserTag {
    private static final long serialVersionUID=1L;

    private Integer id;
    private Integer zone;
    private Integer tagId;

    @ApiModelProperty(value = "添加时间")
    private Date createTime;

    @ApiModelProperty(value = "是否删除,0=未删除,1=删除")
    private Boolean isDel;

}
