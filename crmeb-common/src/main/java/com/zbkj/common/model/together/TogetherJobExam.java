package com.zbkj.common.model.together;


import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zbkj.common.model.user.User;
import com.zbkj.common.model.user.UserTag;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.experimental.Accessors;

import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
@TableName("tb_together_job_exam")
@ApiModel(value = "together_job_exam对象", description = "共创试稿表")
public class TogetherJobExam {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer uid;

    private Integer togetherJobId;

    private Integer suid;

    @ApiModelProperty(value = "1等待试稿，2已发送试稿，3审稿通过，4审稿失败")
    private Integer status;

    @ApiModelProperty(value = "是否删除 状态（0：否，1：是）")
    private Boolean isDel;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "期限")
    private Date limitTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    private String furl;

    @TableField(exist = false)
    private Integer action;

    private Integer limitNum;

    @TableField(exist = false)
    private UserTogether userTogether;

    @TableField(exist = false)
    private UserTag UserTag;


}
