package com.zbkj.common.model.novel;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zbkj.common.model.community.CommunityNotes;
import com.zbkj.common.model.product.Product;
import com.zbkj.common.model.together.UserTogether;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@TableName("tb_novel_adv")
public class NovelAdv {
    private static final long serialVersionUID = 1L;


    @ApiModelProperty(value = "小说id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer type;
    private Integer targetId;

    private Integer isShow;

    private Date createTime;
    /**
     *
     */
    private Date updateTime;

    @TableField(exist = false)
    private CommunityNotes communityNotes;

    @TableField(exist = false)
    private NovelEntity  novel;

    @TableField(exist = false)
    private UserTogether userTogether;

    @TableField(exist = false)
    private Product product;
}
