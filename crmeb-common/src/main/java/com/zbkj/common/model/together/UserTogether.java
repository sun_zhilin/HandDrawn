package com.zbkj.common.model.together;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zbkj.common.model.community.CommunityNotes;
import com.zbkj.common.model.communityOrder.NoteTopic;
import com.zbkj.common.model.user.User;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("tb_user_together")
@ApiModel(value = "共创", description = "共创")
public class UserTogether implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;
    private Integer uid;
    private Integer targetId;
    private Integer replyNum;
    private Integer likeNum;

    @TableField(exist = false)
    private String userName;
    @TableField(exist = false)
    private String userImg;

    @TableField(exist = false)
    private String productIds;

    @TableField(exist = false)
    private List<TogetherJob> togetherJob;

    private String jobIds;

    private String title;

    private String image;



    private String introduction;

    private String videoUrl;

    private String togetherDesc;

    private BigDecimal bondPrice;
    private BigDecimal price;
    private BigDecimal revenuePrice;
    private BigDecimal dividendProportion;

    private Integer togetherModel;
    private Integer type;
    private Integer isPrivate;
    private Integer isProject;
    private Integer isTogether;
    private Integer isPay;


    private Integer status;

    @ApiModelProperty(value = "是否删除 状态（0：否，1：是）")
    private Boolean isDel;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @TableField(exist = false)
    private User user;

    @TableField(exist = false)
    private String fileUrls;

    @TableField(exist = false)
    private String fileName;

    @TableField(exist = false)
    private String openId;

//    private List<TogetherJob>
}
