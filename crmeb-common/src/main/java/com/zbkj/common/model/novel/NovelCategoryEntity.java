package com.zbkj.common.model.novel;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

	import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 小说分类表 Entity 实体类
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * @author: 大粽子
 * +----------------------------------------------------------------------
 * @date Mon Apr 22 15:31:27 CST 2024
 * +----------------------------------------------------------------------
 * @email stivepeim@outlook.com
 * +----------------------------------------------------------------------
 */
@Data
@TableName("tb_novel_category")
public class NovelCategoryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

			/**
			 *
			 */
			@TableId(value = "id", type = IdType.AUTO)
			private Integer id;
			/**
			 * 父级ID
			 */
			private Integer pid;
			/**
			 * 分类名称
			 */
			private String name;
			/**
			 * 是否显示：1-显示，0-不显示
			 */
			private Integer isShow;
			/**
			 * 排序
			 */
			private Integer sort;
			/**
			 * 是否删除，0-未删除，1-删除
			 */
			private Integer isDel;
			/**
			 * 创建时间
			 */
			private Date createTime;
			/**
			 * 更新时间
			 */
			private Date updateTime;

}
