package com.zbkj.common.model.novel;

import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

	import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 小说卷表 Entity 实体类
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * @author: 大粽子
 * +----------------------------------------------------------------------
 * @date Mon Apr 22 15:31:27 CST 2024
 * +----------------------------------------------------------------------
 * @email stivepeim@outlook.com
 * +----------------------------------------------------------------------
 */
@Data
@TableName("tb_novel_roll")
public class NovelRollEntity implements Serializable {
	private static final long serialVersionUID = 1L;

			/**
			 *
			 */
				@TableId
			private Integer id;
			/**
			 * 卷名
			 */
			private String rollName;
			/**
			 * 小说id
			 */
			private Integer novelId;
			/**
			 * 卷数
			 */
			private Integer rollNum;
			/**
			 *
			 */
			private Integer isShow;
			/**
			 *
			 */
			private Date createTime;
			/**
			 *
			 */
			private Date updateTime;

}
