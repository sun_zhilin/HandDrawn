package com.zbkj.common.model.novel;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;

	import java.io.Serializable;
import java.util.Date;
import lombok.Data;

/**
 * 小说历史记录 Entity 实体类
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * @author: 大粽子
 * +----------------------------------------------------------------------
 * @date Mon Apr 22 15:31:27 CST 2024
 * +----------------------------------------------------------------------
 * @email stivepeim@outlook.com
 * +----------------------------------------------------------------------
 */
@Data
@TableName("tb_novel_history")
public class NovelHistoryEntity implements Serializable {
	private static final long serialVersionUID = 1L;

			/**
			 *
			 */
			@TableId(value = "id", type = IdType.AUTO)
			private Integer id;
			/**
			 *
			 */
			private Integer uId;
			/**
			 * 小说id
			 */
			private Integer novelId;
			/**
			 * 章节id
			 */
			private Integer chapterId;
			/**
			 * 页长度（页面内字数
			 */
			private float progress;
			/**
			 *
			 */
			private Integer isShow;
			/**
			 *
			 */
			private Integer isDel;
			/**
			 *
			 */
			private Date createTime;
			/**
			 *
			 */
			private Date updateTime;

}
