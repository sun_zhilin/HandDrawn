package com.zbkj.common.model.travel;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.zbkj.common.model.community.CommunityNotes;
import com.zbkj.common.model.user.User;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
@TableName("tb_travel_apply")
public class TravelApply {
    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableId(value = "note_id")
    private Integer noteId;

    private Integer createBy;

    private String name;

    private String tel;

    @ApiModelProperty(value = "创建时间")
    private Date createTime;

    @ApiModelProperty(value = "更新时间")
    private Date updateTime;

    @ApiModelProperty(value = "是否显示 状态（0：否，1：是）")
    private Boolean isShow;

    @TableField(exist = false)
    private User user;


}
