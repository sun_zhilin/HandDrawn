package com.zbkj.common.constants;

public class TogetherConstants {
    /** 社区评论类型-评论 */
    public static final Integer TOGETHER_REPLY_TYPE_COMMENT = 1;
    /** 社区评论类型-回复 */
    public static final Integer TOGETHER_REPLY_TYPE_REPLY = 2;
}
