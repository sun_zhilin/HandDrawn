package com.zbkj.common.response;

import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class CustomIndexResponse {

    private String name;
    private Integer recId;
    private Integer prdId;
    private Integer status;
    private Integer preOrderId;
    private Integer lastOrderId;
    private Integer productId;
    private CustomUserInfo userInfo;
    private Date finallTime;
    private Date firstTime;
    private Date updateTime;
    private BigDecimal firstMoney;
    private BigDecimal lastMoney;
    private Integer role;
}
