package com.zbkj.common.response;

import lombok.Data;

@Data
public class CustomUserInfo {
    private Integer id;
    private String name;
    private String avatar;
}
