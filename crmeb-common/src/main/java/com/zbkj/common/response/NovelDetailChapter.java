package com.zbkj.common.response;

import io.swagger.models.auth.In;
import lombok.Data;

@Data
public class NovelDetailChapter {

    private Integer id;

    //当前章节
    private Integer index;

    //总章节数
    private Integer chapter;

    private String name;
}
