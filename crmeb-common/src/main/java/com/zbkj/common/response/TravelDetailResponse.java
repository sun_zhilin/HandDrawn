package com.zbkj.common.response;

import com.zbkj.common.model.community.CommunityNotes;
import com.zbkj.common.page.CommonPage;
import lombok.Data;

import java.util.List;

@Data
public class TravelDetailResponse {

//    private List<CommunityNotes> noteList;

    private Integer travelId;

    private String contentImage;

    private CommonPage<CommunityNoteFrontPageResponse> videoList;
}
