package com.zbkj.common.response;

import lombok.Data;

@Data
public class NovelHistoryResponse {
    private Integer bookId;

    private float progress;

    private Integer chapterIndex;
}
