package com.zbkj.common.response;

import lombok.Data;

@Data
public class NovelAllChaptResponse {
    private Integer chapterid;

    //当前章节
    private Integer index;

    //总章节数
    private Integer chapter;

    private String name;
}
