package com.zbkj.common.response;

import lombok.Data;

@Data
public class AuthorityListResponse {

    private Integer uid;

    private String name;

    private String avatar;
}
