package com.zbkj.common.response;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class ProductVideoRespone {

    @ApiModelProperty(value = "商品id")
    private Integer productId;

    @ApiModelProperty(value = "笔记id")
    private Integer noteId;

    @ApiModelProperty(value = "视频地址")
    private String videoUrl;
}
