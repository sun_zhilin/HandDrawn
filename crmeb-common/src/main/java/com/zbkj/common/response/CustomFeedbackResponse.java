package com.zbkj.common.response;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.Date;

@Data
public class CustomFeedbackResponse {

    private String content;
    @ApiModelProperty(value = "创建时间")
    private Date updateTime;
}
