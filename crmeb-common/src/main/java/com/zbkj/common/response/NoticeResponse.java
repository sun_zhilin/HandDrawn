package com.zbkj.common.response;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.zbkj.common.model.order.OrderInvoiceDetail;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value = "NoticeResponse", description = "通知响应对象")
public class NoticeResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    /** 通知id */
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /** 商品类型通知，0则为自定义通知 */
    @ApiModelProperty(value = "typeId")
    private Long typeId;

    /** 用户id */
    @ApiModelProperty(value = "用户id")
    private Long userId;

    /** 订单号 */
    @ApiModelProperty(value = "订单号")
    private String orderNo;

    @ApiModelProperty(value = "商品图片")
    private String image;

    /** 内容 */
    @ApiModelProperty(value = "内容")
    private String Content;
    /** 标题 */
    @ApiModelProperty(value = "标题")
    private String Title;

    @ApiModelProperty(value = "通知时间")
    private Date createTime;
}
