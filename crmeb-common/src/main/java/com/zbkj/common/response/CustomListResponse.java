package com.zbkj.common.response;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.math.BigDecimal;
import java.util.Date;

@Data
public class CustomListResponse {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "定制id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String name;
    private Integer recId;
    private Integer prdId;
    private Integer status;
    private String preOrderId;
    private String lastOrderId;
//    private Boolean isShow;
    private Integer feedback_time;
//    private Integer productId;
    private  String imgUrl;
    private BigDecimal firstMoney;
    private BigDecimal lastMoney;

}
