package com.zbkj.common.response;

import lombok.Data;

@Data
public class NovelCategoryResponse {
    private Integer id;
    private String name;
}
