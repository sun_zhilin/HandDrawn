package com.zbkj.common.response;

import io.swagger.models.auth.In;
import lombok.Data;

import java.util.Date;
import java.util.List;

@Data
public class NovelDetailResponse {

    private String author;
    private Integer id;
    private String c_name;
    private String lastzj;
    private String name;
    private String pic;
    private String status;
    private String wordCount;
    private List<NovelDetailChapter> lastzjlist;
    private String progress;
    private Date lasttime;
    private String intro;
}
