package com.zbkj.common.response;

import lombok.Data;

@Data
public class NovelContentResponse {

    private boolean canread;

    private String content;
}
