package com.zbkj.common.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@ApiModel(value="CommunityNoteCollectRespone", description="社区笔记收藏响应对象")
public class CommunityNoteCollectionRespone implements Serializable {

    private static final long serialVersionUID = -8121525449704982702L;

    @ApiModelProperty("笔记ID")
    private Integer id;

    @ApiModelProperty("封面")
    private String cover;

    @ApiModelProperty("标题")
    private String title;

    @ApiModelProperty("文章类型")
    private Integer type;

}
