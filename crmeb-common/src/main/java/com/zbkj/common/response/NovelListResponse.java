package com.zbkj.common.response;

import lombok.Data;

@Data
public class NovelListResponse {
    private Integer id;
    /**
     * 书名
     */
    private String articlename;
    /**
     * 简介
     */
    private String intro;
    /**
     * 封面路径
     */
    private String urlImg;
//    作者名字
    private String author;
    private String avatar;
}
