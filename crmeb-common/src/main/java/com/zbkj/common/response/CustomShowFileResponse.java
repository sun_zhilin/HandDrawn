package com.zbkj.common.response;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class CustomShowFileResponse {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "附件id")
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private Integer customId;

    private String fileUrl;

    private String fileName;

    private String fileType;
}
