package com.zbkj.common.response;

import lombok.Data;

@Data
public class CommonPickerResponse {
    private String text;
    private Integer value;
}
