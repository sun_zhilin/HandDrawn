package com.zbkj.common.response;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;

@Data
public class TravelListResponse {

    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    private String title;
    private String intro;
    private String cover;
    private String content_image;
}
