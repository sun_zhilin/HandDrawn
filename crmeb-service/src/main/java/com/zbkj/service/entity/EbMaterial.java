package com.zbkj.service.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

import java.io.Serializable;

/**
 * 素材附件(EbMaterial)实体类
 *
 * @author makejava
 * @since 2024-03-19 11:04:43
 */
@Data
@EqualsAndHashCode(callSuper = false)
@Accessors(chain = true)
@TableName("eb_material")
public class EbMaterial implements Serializable {
    private static final long serialVersionUID = 1L;
    /**
     * 素材附件id
     */
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 商品id
     */
    private Integer productId;

    /**
     * 附件地址
     */
    private String url;
    /**
     * 文件名字
     */
    private String name;
}
