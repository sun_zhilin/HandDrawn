package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.communityOrder.UserNote;
import com.zbkj.service.dao.UserNoteDao;
import com.zbkj.service.service.ActivityStyleService;
import com.zbkj.service.service.UserNoteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class UserNoteServiceImpl extends ServiceImpl<UserNoteDao, UserNote> implements UserNoteService {

    private static final Logger logger = LoggerFactory.getLogger(ActivityStyleService.class);
}
