package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.together.UserTogetherProduct;
import com.zbkj.common.model.together.UserTogetherRecord;

public interface UserTogetherRecordService extends IService<UserTogetherRecord> {

}
