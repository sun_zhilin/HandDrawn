package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.exception.CrmebException;
import com.zbkj.common.model.custom.Custom;
import com.zbkj.common.model.custom.CustomFeedback;
import com.zbkj.common.model.user.User;
import com.zbkj.common.request.CustomAddRequest;
import com.zbkj.common.request.CustomFeedbackRequest;
import com.zbkj.common.response.CustomFeedbackResponse;
import com.zbkj.service.dao.CustomFeedbackDao;
import com.zbkj.service.service.CustomFeedbackService;
import com.zbkj.service.service.CustomService;
import com.zbkj.service.service.UserService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.support.TransactionTemplate;

import javax.annotation.Resource;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CustomFeedbackServiceImpl extends ServiceImpl<CustomFeedbackDao, CustomFeedback> implements CustomFeedbackService {

    @Resource
    CustomFeedbackDao dao;

    @Autowired
    CustomService customService;
    @Autowired
    TransactionTemplate transactionTemplate;

    @Autowired
    UserService userService;

    @Override
    public Integer add(CustomAddRequest request) throws ParseException {
        User user = userService.getInfo();
        if(!request.getProId().equals(user.getId()))
            throw new CrmebException("用户id不匹配");
        Custom custom = new Custom();
        BeanUtils.copyProperties(request,custom);
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm"); // 定义日期时间格式
        custom.setFirstTime(sdf.parse(request.getFirstTime()));
        custom.setFinallTime(sdf.parse(request.getFirstTime()));
        custom.setPrdId(request.getProId());
//        custom.setStatus(0);
        custom.setFeedback_time(request.getFeedbackTime());
        customService.save(custom);
        return custom.getId();
    }

    @Override
    public List<CustomFeedbackResponse> loadFeedback(Integer id) {
        LambdaQueryWrapper<CustomFeedback> lqw = new LambdaQueryWrapper<>();
        lqw.eq(CustomFeedback::getCustomId,id);
        lqw.eq(CustomFeedback::getIsShow,1);
        lqw.eq(CustomFeedback::getIsDel,0);
        lqw.orderByAsc(CustomFeedback::getId);

        List<CustomFeedback> list = dao.selectList(lqw);
        List<CustomFeedbackResponse> responseList = list.stream().map(item ->{
            CustomFeedbackResponse response = new CustomFeedbackResponse();
            BeanUtils.copyProperties(item,response);
            return  response;
        }).collect(Collectors.toList());
        return responseList;
    }

    @Override
    public String addFeedback(CustomFeedbackRequest request) {
        CustomFeedback feedback = new CustomFeedback();
        BeanUtils.copyProperties(request,feedback);
        Custom custom = customService.getById(request.getCustomId());
        Date date = new Date(System.currentTimeMillis()+custom.getFeedback_time()*24*3600*1000);
        custom.setNextFeedTime(date);

        Boolean execute = transactionTemplate.execute(e->{
            customService.updateById(custom);
            save(feedback);
            return Boolean.TRUE;
        });
        if(!execute)
            throw new CrmebException("更新数据出错");

        return "进度反馈成功";
    }
}
