package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.custom.Custom;
import com.zbkj.common.request.CustomPreorderRequest;
import com.zbkj.common.request.CustomUpdateRequest;
import com.zbkj.common.request.PreOrderRequest;
import com.zbkj.common.response.CommonPickerResponse;
import com.zbkj.common.response.CustomIndexResponse;
import com.zbkj.common.response.CustomListResponse;
import com.zbkj.common.response.OrderNoResponse;
import com.zbkj.common.vo.MyRecord;

import java.util.List;

public interface CustomService extends IService<Custom> {
    CustomIndexResponse index(Integer id);

    PreOrderRequest preOrder(PreOrderRequest request);

    String updateStatue(CustomUpdateRequest request);

//    List<CommonPickerResponse> getCustom();

    String join(Integer id);

    List<CustomListResponse> getlist();
}
