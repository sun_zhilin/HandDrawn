package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.communityOrder.CommunityOrder;
import com.zbkj.common.model.together.TogetherJob;

public interface TogetherJobService extends IService<TogetherJob> {

}
