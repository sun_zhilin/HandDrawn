package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.novel.NovelAuthoritytEntity;
import com.zbkj.service.dao.NovelAuthorityDao;
import com.zbkj.service.service.NovelAuthorityService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service("novelAuthorityService")
public class NovelAuthorityServiceImpl extends ServiceImpl<NovelAuthorityDao, NovelAuthoritytEntity> implements NovelAuthorityService {
    @Resource
    NovelAuthorityDao dao;
    @Override
    public List<NovelAuthoritytEntity> getAllByNovelId(Integer novelId) {

        return dao.getAllByNovelId(novelId);
    }
}
