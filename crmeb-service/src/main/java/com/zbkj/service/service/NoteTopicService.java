package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.model.communityOrder.NoteTopic;

import com.zbkj.common.request.NoteTopicRequest;
import com.zbkj.common.request.PageParamRequest;


import java.util.List;


public interface NoteTopicService  extends IService<NoteTopic> {
    /**
     * 笔记专题列表
     * @param pageParamRequest 分页参数
     * @return PageInfo
     */
    PageInfo<NoteTopic> getList(PageParamRequest pageParamRequest);

    /**
     * 新增笔记专题
     * @param NoteTopicRequest 标签参数
     */
    Boolean create(NoteTopicRequest NoteTopicRequest);

    /**
     * 删除笔记专题
     * @param id 标签id
     */
    Boolean delete(Integer id);

    /**
     * 修改笔记专题
     * @param NoteTopicRequest 标签参数
     */
    Boolean updateTag(NoteTopicRequest NoteTopicRequest);

    /**
     * 所有笔记专题列表
     * @return List
     */
    List<NoteTopic> getAllList();
}
