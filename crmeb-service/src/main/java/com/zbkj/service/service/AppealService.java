package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.Appeal;

public interface AppealService extends IService<Appeal> {
}
