package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.notice.NoticeType;

import java.util.List;

/**
 * 【请填写功能名称】Service接口
 *
 * @author my
 * @date 2024-04-07
 */
public interface INoticeTypeService extends IService<NoticeType>
{

}
