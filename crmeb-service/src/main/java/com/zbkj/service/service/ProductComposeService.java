package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.product.ProductCompose;

public interface ProductComposeService extends IService<ProductCompose> {
}
