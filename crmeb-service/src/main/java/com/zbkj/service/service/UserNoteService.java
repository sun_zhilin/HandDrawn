package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.communityOrder.UserNote;
import com.zbkj.common.model.user.User;

public interface UserNoteService extends IService<UserNote> {
}
