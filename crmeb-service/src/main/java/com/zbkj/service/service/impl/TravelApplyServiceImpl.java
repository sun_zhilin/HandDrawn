package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.travel.TravelApply;
import com.zbkj.service.dao.TravelApplyMapper;
import com.zbkj.service.service.TravelApplyService;
import org.springframework.stereotype.Service;

@Service
public class TravelApplyServiceImpl extends ServiceImpl<TravelApplyMapper, TravelApply> implements TravelApplyService {
}
