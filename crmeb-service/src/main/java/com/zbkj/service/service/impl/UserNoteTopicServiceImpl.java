package com.zbkj.service.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.zbkj.common.model.communityOrder.UserNoteTopic;

import com.zbkj.service.dao.UserNoteTopicDao;
import com.zbkj.service.service.ActivityStyleService;

import com.zbkj.service.service.UserNoteTopicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

@Service
public class UserNoteTopicServiceImpl extends ServiceImpl<UserNoteTopicDao, UserNoteTopic> implements UserNoteTopicService {

    private static final Logger logger = LoggerFactory.getLogger(ActivityStyleService.class);

}
