package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.product.ProductUser;

public interface ProductUserService extends IService<ProductUser> {
}
