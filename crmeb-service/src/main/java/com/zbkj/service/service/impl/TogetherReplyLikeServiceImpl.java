package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.community.CommunityReplyLike;
import com.zbkj.common.model.together.TogetherReplyLike;
import com.zbkj.service.dao.TogetherReplyLikeDao;
import com.zbkj.service.service.TogetherReplyLikeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TogetherReplyLikeServiceImpl  extends ServiceImpl<TogetherReplyLikeDao, TogetherReplyLike> implements TogetherReplyLikeService {

    @Autowired
    TogetherReplyLikeDao dao;
    @Override
    public List<TogetherReplyLike> findListByNoteIdAndUid(Integer togetherId, Integer userId) {
        LambdaQueryWrapper<TogetherReplyLike> lqw = Wrappers.lambdaQuery();
        lqw.eq(TogetherReplyLike::getTogetherId, togetherId);
        lqw.eq(TogetherReplyLike::getUid, userId);
        return dao.selectList(lqw);
    }

    @Override
    public TogetherReplyLike getDetail(Integer replyId, Integer userId) {
        LambdaQueryWrapper<TogetherReplyLike> lqw = Wrappers.lambdaQuery();
        lqw.eq(TogetherReplyLike::getReplyId, replyId);
        lqw.eq(TogetherReplyLike::getUid, userId);
        lqw.last("limit 1");
        return getOne(lqw);
    }
}
