package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.model.together.TogetherReply;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.TogetherReplyRequest;
import com.zbkj.common.response.TogetherReplyResponse;

import java.util.List;

public interface TogetherReplyService extends IService<TogetherReply> {
    TogetherReplyResponse add(TogetherReplyRequest togetherReply);

    Integer deleteReply(Integer replyId);

    PageInfo<TogetherReplyResponse> findNoteReplyPageList(Integer togetherId, PageParamRequest request);

    void likeReply(Integer replyId);
}
