package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.product.ProductCompose;
import com.zbkj.service.dao.ProductComposeMapper;
import com.zbkj.service.service.ProductComposeService;
import org.springframework.stereotype.Service;

@Service
public class ProductComposeServiceImpl extends ServiceImpl<ProductComposeMapper, ProductCompose> implements ProductComposeService {
}
