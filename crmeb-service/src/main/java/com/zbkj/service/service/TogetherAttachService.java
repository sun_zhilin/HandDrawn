package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.together.TogetherAttach;

public interface TogetherAttachService extends IService<TogetherAttach> {

}
