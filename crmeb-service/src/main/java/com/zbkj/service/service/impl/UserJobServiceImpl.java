package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.user.UserJob;
import com.zbkj.service.dao.UserJobDao;
import com.zbkj.service.service.UserJobService;
import org.springframework.stereotype.Service;

@Service
public class UserJobServiceImpl extends ServiceImpl<UserJobDao, UserJob> implements UserJobService {
}
