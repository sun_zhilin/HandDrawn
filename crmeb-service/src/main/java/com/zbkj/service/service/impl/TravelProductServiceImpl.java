package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.travel.TravelProduct;
import com.zbkj.service.dao.TravelProductDao;
import com.zbkj.service.service.TravelProductService;
import org.springframework.stereotype.Service;


@Service
public class TravelProductServiceImpl extends ServiceImpl<TravelProductDao, TravelProduct> implements TravelProductService {
}

