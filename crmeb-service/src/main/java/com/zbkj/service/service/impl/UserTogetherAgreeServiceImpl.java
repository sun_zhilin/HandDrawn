package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.together.UserTogetherAgree;
import com.zbkj.service.dao.UserTogetherAgreeDao;
import com.zbkj.service.service.UserTogetherAgreeService;
import org.springframework.stereotype.Service;

@Service
public class UserTogetherAgreeServiceImpl extends ServiceImpl<UserTogetherAgreeDao, UserTogetherAgree> implements UserTogetherAgreeService {

}
