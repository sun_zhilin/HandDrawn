package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.product.ProductTag;
import com.zbkj.common.model.product.ProductUser;
import com.zbkj.service.dao.ProductTagDao;
import com.zbkj.service.dao.ProductUserMapper;
import com.zbkj.service.service.ProductTagService;
import com.zbkj.service.service.ProductUserService;
import org.springframework.stereotype.Service;

@Service
public class ProductUserServiceImpl extends ServiceImpl<ProductUserMapper, ProductUser> implements ProductUserService {
}
