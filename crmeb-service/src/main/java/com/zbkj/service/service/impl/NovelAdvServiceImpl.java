package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.model.notice.NoticeType;
import com.zbkj.common.model.novel.NovelAdv;
import com.zbkj.common.model.together.UserTogether;
import com.zbkj.common.model.user.User;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.service.dao.NoticeTypeMapper;
import com.zbkj.service.dao.NovelAdvDao;
import com.zbkj.service.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class NovelAdvServiceImpl extends ServiceImpl<NovelAdvDao, NovelAdv> implements NovelAdvService {

    @Autowired
    private CommunityNotesService communityNotesService;

    @Autowired
    private NovelService novelService;

    @Autowired
    private UserTogetherService userTogetherService;

    @Autowired
    private ProductService productService;
    @Override
    public PageInfo<NovelAdv> getAdvRandom(PageParamRequest request) {
        Integer i = (int) (Math.random()*2);
        LambdaQueryWrapper<NovelAdv> lqw = new LambdaQueryWrapper<>();
        if(i.equals(1)){
            lqw.eq(NovelAdv::getIsShow,1);
            lqw.eq(NovelAdv::getType,1);

        }else {
            lqw.eq(NovelAdv::getIsShow,1);
            lqw.gt(NovelAdv::getType,1);
            lqw.lt(NovelAdv::getType,5);
        }
        Page<NovelAdv> page =PageHelper.startPage(request.getPage(), request.getLimit());
        List<NovelAdv> list = list(lqw);
        list.forEach(item->{
            switch (item.getType()){
                case 1:
                    item.setCommunityNotes(communityNotesService.getByIdException(item.getTargetId()));
                    break;
                case 2:
                    item.setNovel(novelService.getById(item.getTargetId()));
                    break;
                case 3:
                    item.setUserTogether(userTogetherService.getById(item.getTargetId()));
                    break;
                case 4:
                    item.setProduct(productService.getById(item.getTargetId()));
                    break;
            }
        });
        return CommonPage.copyPageInfo(page,list);
    }

    @Override
    public PageInfo<NovelAdv> getAdvType(Integer type, PageParamRequest request) {
        LambdaQueryWrapper<NovelAdv> lqw = new LambdaQueryWrapper<>();
        lqw.eq(NovelAdv::getType,type);
        lqw.eq(NovelAdv::getIsShow,1);
        Page<NovelAdv> page;
        if(type==1){
            page = PageHelper.startPage(request.getPage(),1);
        }else {
            page = PageHelper.startPage(request.getPage(),request.getLimit());
        }
        List<NovelAdv> list = list(lqw);
        list.forEach(item->{
            switch (item.getType()){
                case 1:
                    item.setCommunityNotes(communityNotesService.getByIdException(item.getTargetId()));
                    break;
                case 2:
                    item.setNovel(novelService.getById(item.getTargetId()));
                    break;
                case 3:
                    item.setUserTogether(userTogetherService.getById(item.getTargetId()));
                    break;
                case 4:
                    item.setProduct(productService.getById(item.getTargetId()));
                    break;
            }
        });
        return CommonPage.copyPageInfo(page,list);
    }

}
