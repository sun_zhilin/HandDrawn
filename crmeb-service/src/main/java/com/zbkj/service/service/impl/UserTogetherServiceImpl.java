package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.communityOrder.CommunityOrder;
import com.zbkj.common.model.together.UserTogether;

import com.zbkj.service.dao.UserTogetherDao;
import com.zbkj.service.service.CommunityOrderService;
import com.zbkj.service.service.UserTogetherService;
import org.springframework.stereotype.Service;

@Service
public class UserTogetherServiceImpl extends ServiceImpl<UserTogetherDao, UserTogether> implements UserTogetherService {

}
