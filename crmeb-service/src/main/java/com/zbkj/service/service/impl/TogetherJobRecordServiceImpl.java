package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.together.TogetherJobRecord;
import com.zbkj.common.model.together.TogetherProductOrder;
import com.zbkj.service.dao.TogetherJobRecordDao;
import com.zbkj.service.dao.TogetherProductOrderDao;
import com.zbkj.service.service.TogetherJobRecordService;
import com.zbkj.service.service.TogetherProductOrderService;
import org.springframework.stereotype.Service;

@Service
public class TogetherJobRecordServiceImpl extends ServiceImpl<TogetherJobRecordDao, TogetherJobRecord> implements TogetherJobRecordService {
}
