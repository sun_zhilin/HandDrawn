package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.travel.Travel;
import com.zbkj.common.model.travel.TravelTopic;
import com.zbkj.service.dao.TravelDao;
import com.zbkj.service.dao.TravelTopicDao;
import com.zbkj.service.service.TravelService;
import com.zbkj.service.service.TravelTopicService;
import org.springframework.stereotype.Service;

@Service
public class TravelTopicServiceImpl extends ServiceImpl<TravelTopicDao, TravelTopic> implements TravelTopicService {
}
