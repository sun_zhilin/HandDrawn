package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.model.novel.NovelEntity;
import com.zbkj.common.request.NovelAddRequest;
import com.zbkj.common.request.NovelListRequest;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.response.*;
//import com.zbkj.common.utils.PageUtils;

import java.util.List;

/**
 * 小说对象表 业务接口
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * @author: 大粽子
 * +----------------------------------------------------------------------
 * @date Mon Apr 22 15:31:27 CST 2024
 * +----------------------------------------------------------------------
 * @email stivepeim@outlook.com
 * +----------------------------------------------------------------------
 */
public interface NovelService extends IService<NovelEntity> {
    List<NovelListResponse> index();
    PageInfo<NovelListResponse> index(NovelListRequest listRequest);

    NovelDetailResponse getDetail(Integer id);
    List<NovelAllChaptResponse> getAllChapter(Integer id);
    List<NovelAllChaptResponse> getAllDraft(Integer id);

    Integer add(NovelAddRequest request);

    String addAuthority(Integer uname,Integer novelId);

    String delAuthority(Integer uid, Integer novelId);

    List<NovelListResponse> findByName(String articlename);

    /**
     * 模糊查询小说
     *
     * @param request
     * @param keyword
     * @return
     */
    PageInfo<NovelListResponse> findByKeyword(PageParamRequest request, String keyword);

    List<NovelListResponse> mylist();

    List<AuthorityListResponse> showAuthority(Integer novelId);

    List<NovelListResponse> myAuthor();

    List<NovelAllChaptResponse> getGarbage(Integer id);
    /**
            * Novel 列表查询
        * @param request 默认是是体类 根据自己需求修改或者创建自己的request
        * @param pageParamRequest 分页参数对象
        * @return
        */
//    List<Novel> getList(NovelEntity request, PageParamRequest pageParamRequest)
}

