package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.community.CommunityCategory;
import com.zbkj.common.model.openim.ImOpenimUser;
import com.zbkj.service.dao.ImOpenimUserDao;
import com.zbkj.service.dao.community.CommunityCategoryDao;
import com.zbkj.service.service.CommunityCategoryService;
import com.zbkj.service.service.ImOpenimUserService;
import org.springframework.stereotype.Service;

@Service
public class ImOpenimUserServiceImpl extends ServiceImpl<ImOpenimUserDao, ImOpenimUser> implements ImOpenimUserService {
}
