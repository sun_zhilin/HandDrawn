package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.user.UserIntegralRecord;
import com.zbkj.common.model.user.UserJob;

public interface UserJobService extends IService<UserJob> {
}
