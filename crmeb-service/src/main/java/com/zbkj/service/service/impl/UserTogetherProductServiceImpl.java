package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.together.UserTogether;
import com.zbkj.common.model.together.UserTogetherProduct;
import com.zbkj.service.dao.UserTogetherDao;
import com.zbkj.service.dao.UserTogetherProductDao;
import com.zbkj.service.service.UserTogetherProductService;
import com.zbkj.service.service.UserTogetherService;
import org.springframework.stereotype.Service;

@Service
public class UserTogetherProductServiceImpl extends ServiceImpl<UserTogetherProductDao, UserTogetherProduct> implements UserTogetherProductService {
}
