package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.model.community.CommunityNotes;
import com.zbkj.common.model.communityOrder.CommunityOrder;
import com.zbkj.common.model.communityOrder.NoteTopic;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.service.dao.CommunityOrderDao;
import com.zbkj.service.service.CommunityNotesService;
import com.zbkj.service.service.CommunityOrderService;
import com.zbkj.service.service.NoteTopicService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;
import javax.annotation.Resource;
import java.util.List;

@Service
public class CommunityOrderServiceImpl extends ServiceImpl<CommunityOrderDao, CommunityOrder> implements CommunityOrderService {
    @Resource
    private CommunityOrderDao dao;

    @Resource
    private CommunityNotesService notesService;


    @Resource
    private NoteTopicService noteTopicService;
    private static final Logger logger = LoggerFactory.getLogger(CommunityOrderService.class);


    @Override
    public PageInfo<CommunityOrder> getList(PageParamRequest pageParamRequest,CommunityOrder communityOrder) {
        Page<Object> page = PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());
        LambdaQueryWrapper<CommunityOrder> lqw = Wrappers.lambdaQuery();
        if (communityOrder.getOrderSn()!=null && !communityOrder.equals("")) {
            lqw.eq(CommunityOrder::getOrderSn,communityOrder.getOrderSn());
        }
        lqw.eq(CommunityOrder::getTargetType,communityOrder.getTargetType());
        lqw.orderByDesc(CommunityOrder::getId);
        List<CommunityOrder> tagList = dao.selectList(lqw);
        for (CommunityOrder order:tagList) {
            if (order.getTargetType()==0) {
                CommunityNotes byId = notesService.getById(order.getTargetId());
                order.setNotes(byId);

            }else{
                NoteTopic byId = noteTopicService.getById(order.getTargetId());
                order.setNoteTopic(byId);
            }
        }
        return CommonPage.copyPageInfo(page, tagList);
    }
}
