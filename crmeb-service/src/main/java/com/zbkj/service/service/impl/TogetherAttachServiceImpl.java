package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.template.TemplateMessage;
import com.zbkj.common.model.together.TogetherAttach;
import com.zbkj.service.dao.TemplateMessageDao;
import com.zbkj.service.dao.TogetherAttachDao;
import com.zbkj.service.service.TemplateMessageService;
import com.zbkj.service.service.TogetherAttachService;
import org.springframework.stereotype.Service;

@Service
public class TogetherAttachServiceImpl extends ServiceImpl<TogetherAttachDao, TogetherAttach> implements TogetherAttachService {
}
