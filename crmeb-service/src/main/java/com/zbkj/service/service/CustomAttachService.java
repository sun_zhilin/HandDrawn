package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.custom.CustomAttach;
import com.zbkj.common.request.CustomAttachUploadRequest;
import com.zbkj.common.response.CustomShowFileResponse;
import com.zbkj.common.vo.MyRecord;

import java.util.List;

public interface CustomAttachService extends IService<CustomAttach> {
    List<CustomShowFileResponse> showFile(Integer id,Integer type);

    String uploadFile(CustomAttachUploadRequest request);
}
