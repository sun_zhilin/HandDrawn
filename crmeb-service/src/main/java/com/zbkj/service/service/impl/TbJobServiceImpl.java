package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.together.TbJob;
import com.zbkj.service.dao.TbJobDao;
import com.zbkj.service.service.TbJobService;
import org.springframework.stereotype.Service;

@Service
public class TbJobServiceImpl extends ServiceImpl<TbJobDao, TbJob> implements TbJobService {
}
