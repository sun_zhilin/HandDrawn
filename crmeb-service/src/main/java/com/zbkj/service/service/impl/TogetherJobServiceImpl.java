package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.together.TogetherJob;
import com.zbkj.service.dao.TogetherJobDao;
import com.zbkj.service.service.TogetherJobService;
import org.springframework.stereotype.Service;

@Service
public class TogetherJobServiceImpl extends ServiceImpl<TogetherJobDao, TogetherJob> implements TogetherJobService {
}
