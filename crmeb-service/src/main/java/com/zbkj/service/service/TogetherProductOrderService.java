package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;

import com.zbkj.common.model.together.TogetherProductOrder;
import com.zbkj.common.model.together.UserTogetherAgree;

public interface TogetherProductOrderService extends IService<TogetherProductOrder> {
}
