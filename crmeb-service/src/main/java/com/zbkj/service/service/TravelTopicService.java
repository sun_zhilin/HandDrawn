package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.travel.Travel;
import com.zbkj.common.model.travel.TravelTopic;

public interface TravelTopicService extends IService<TravelTopic> {
}
