package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.custom.CustomFeedback;
import com.zbkj.common.request.CustomAddRequest;
import com.zbkj.common.request.CustomFeedbackRequest;
import com.zbkj.common.response.CustomFeedbackResponse;
import com.zbkj.common.vo.MyRecord;

import java.text.ParseException;
import java.util.List;

public interface CustomFeedbackService extends IService<CustomFeedback> {
    List<CustomFeedbackResponse> loadFeedback(Integer id);

    String addFeedback(CustomFeedbackRequest request);

    Integer add(CustomAddRequest request) throws ParseException;
}
