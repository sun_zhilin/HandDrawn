package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.model.product.ProductRelation;
import com.zbkj.common.model.user.UserNoteCollect;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.CancelNoteCollectRequest;
import com.zbkj.common.request.NoteCollectRequest;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.response.CommunityNoteCollectionRespone;
import com.zbkj.common.utils.CrmebUtil;
import com.zbkj.service.dao.EbMaterialDao;
import com.zbkj.service.dao.EbUserNoteCollectionDao;
import com.zbkj.service.entity.EbUserNoteCollection;
import com.zbkj.service.service.EbUserNoteCollectionService;
import com.zbkj.service.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;
import java.util.List;

/**
 * 用户收藏note(EbUserNoteCollection)表服务实现类
 *
 * @author makejava
 * @since 2024-03-19 16:04:41
 */
@Service("ebUserNoteCollectionService")
public class EbUserNoteCollectionServiceImpl extends ServiceImpl<EbUserNoteCollectionDao, EbUserNoteCollection> implements EbUserNoteCollectionService {



    @Resource
    private EbUserNoteCollectionDao dao;

    @Autowired
    private UserService userService;

    @Override
    public Boolean add(NoteCollectRequest request) {
        EbUserNoteCollection ebUserNoteCollection = new EbUserNoteCollection();
        ebUserNoteCollection.setNoteId(request.getNoteId());
        ebUserNoteCollection.setUserId(userService.getUserIdException());
        ebUserNoteCollection.setCreateTime(new Date());
        return save(ebUserNoteCollection);
    }

    @Override
    public Boolean delete(CancelNoteCollectRequest request) {
        Integer userId = userService.getUserIdException();
        LambdaQueryWrapper<EbUserNoteCollection> lqw = Wrappers.lambdaQuery();
        lqw.in(EbUserNoteCollection::getNoteId, CrmebUtil.stringToArray(request.getIds()));
        lqw.eq(EbUserNoteCollection::getUserId, userId);
        return dao.delete(lqw) > 0;
    }

    @Override
    public PageInfo<CommunityNoteCollectionRespone> getUserList(PageParamRequest pageParamRequest) {
        Integer userId = userService.getUserIdException();
        Page<Object> page = PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());
        List<CommunityNoteCollectionRespone> list = dao.getUserList(userId) ;
        return CommonPage.copyPageInfo(page,list);
    }
}

