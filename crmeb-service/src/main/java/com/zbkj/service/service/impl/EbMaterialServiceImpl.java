package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.zbkj.common.exception.CrmebException;
import com.zbkj.common.model.order.Order;
import com.zbkj.common.model.order.OrderDetail;
import com.zbkj.common.model.user.User;
import com.zbkj.common.vo.FileResultVo;
import com.zbkj.service.entity.EbMaterial;
import com.zbkj.service.dao.EbMaterialDao;
import com.zbkj.service.service.EbMaterialService;
import com.zbkj.service.service.OrderDetailService;
import com.zbkj.service.service.OrderService;
import com.zbkj.service.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

/**
 * 素材附件(EbMaterial)表服务实现类
 *
 * @author makejava
 * @since 2024-03-19 11:04:45
 */
@Service
public class EbMaterialServiceImpl implements EbMaterialService {
    @Resource
    private EbMaterialDao ebMaterialDao;

    @Autowired
    private OrderService orderService;

    @Autowired
    private UserService userService;

    @Autowired
    private OrderDetailService orderDetailService;
    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    @Override
    public EbMaterial queryById(Integer id) {
        return ebMaterialDao.queryById(id);
    }

    @Override
    public EbMaterial queryByProductId(Integer productId) {

        return ebMaterialDao.queryByProductId(productId);
    }

    @Override
    public List<FileResultVo> download(String preOrderNo) {
        //确订单信息
        User currentUser = userService.getInfo();
        Order order = orderService.getByOrderNo(preOrderNo);
        if (order.getIsUserDel() || order.getIsMerchantDel() || !order.getUid().equals(currentUser.getId())) {
            throw new CrmebException("订单不存在");
        }

        List<OrderDetail> orderDetailList = orderDetailService.getByOrderNo(preOrderNo);
        List<FileResultVo> fileResultVoList = new ArrayList<>();
        //查找下载地址
        for(OrderDetail orderDetail : orderDetailList){
            LambdaQueryWrapper<EbMaterial> lqw2 = Wrappers.lambdaQuery();
            lqw2.eq(EbMaterial::getProductId,orderDetail.getProductId());
            EbMaterial ebMaterial = ebMaterialDao.selectOne(lqw2);
            FileResultVo fileResultVo = new FileResultVo();
            fileResultVo.setFileName(ebMaterial.getName());
            fileResultVo.setUrl(ebMaterial.getUrl());
            fileResultVoList.add(fileResultVo);
        }
        return fileResultVoList;


    }

    /**
     * 分页查询
     *
     * @param ebMaterial 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
    @Override
    public Page<EbMaterial> queryByPage(EbMaterial ebMaterial, PageRequest pageRequest) {
        long total = this.ebMaterialDao.count(ebMaterial);
        return new PageImpl<>(this.ebMaterialDao.queryAllByLimit(ebMaterial, pageRequest), pageRequest, total);
    }

    /**
     * 新增数据
     *
     * @param ebMaterial 实例对象
     * @return 实例对象
     */
    @Override
    public EbMaterial insert(EbMaterial ebMaterial) {
        ebMaterialDao.insert(ebMaterial);
        return ebMaterial;
    }

    /**
     * 修改数据
     *
     * @param ebMaterial 实例对象
     * @return 实例对象
     */
    @Override
    public EbMaterial update(EbMaterial ebMaterial) {
        this.ebMaterialDao.update(ebMaterial);
        return this.queryById(ebMaterial.getId());
    }

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    @Override
    public boolean deleteById(Integer id) {
        return this.ebMaterialDao.deleteById(id) > 0;
    }
}
