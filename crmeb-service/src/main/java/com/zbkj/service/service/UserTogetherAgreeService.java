package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.together.UserTogetherAgree;

public interface UserTogetherAgreeService  extends IService<UserTogetherAgree> {
}
