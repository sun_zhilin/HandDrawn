package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.model.novel.NovelAdv;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;

import java.util.List;

public interface NovelAdvService extends IService<NovelAdv> {
    PageInfo<NovelAdv> getAdvRandom(PageParamRequest request);
    PageInfo<NovelAdv> getAdvType(Integer type,PageParamRequest request);
}
