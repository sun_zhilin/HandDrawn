package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.novel.NovelRollEntity;
//import com.zbkj.common.utils.PageUtils;

import java.util.Map;

/**
 * 小说卷表 业务接口
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * @author: 大粽子
 * +----------------------------------------------------------------------
 * @date Mon Apr 22 15:31:27 CST 2024
 * +----------------------------------------------------------------------
 * @email stivepeim@outlook.com
 * +----------------------------------------------------------------------
 */
public interface NovelRollService extends IService<NovelRollEntity> {

            /**
            * NovelRoll 列表查询
        * @param request 默认是是体类 根据自己需求修改或者创建自己的request
        * @param pageParamRequest 分页参数对象
        * @return
        */
//    List<NovelRoll> getList(NovelRollEntity request, PageParamRequest pageParamRequest)
}

