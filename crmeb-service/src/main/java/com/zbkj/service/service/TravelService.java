package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.model.travel.Travel;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.TravelDetailRequest;
import com.zbkj.common.request.TravelShopRequest;
import com.zbkj.common.response.ProductFrontResponse;
import com.zbkj.common.response.TravelDetailResponse;
import com.zbkj.common.response.TravelListResponse;
import com.zbkj.common.vo.MyRecord;

import java.util.List;

public interface TravelService extends IService<Travel> {
    List<TravelListResponse> index();

    TravelDetailResponse detail(TravelDetailRequest request);

    PageInfo<ProductFrontResponse> shop(TravelShopRequest request);
}
