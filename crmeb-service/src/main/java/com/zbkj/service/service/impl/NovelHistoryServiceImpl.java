package com.zbkj.service.service.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zbkj.common.exception.CrmebException;
import com.zbkj.common.model.novel.NovelHistoryEntity;
import com.zbkj.common.model.novel.NovelRollChapterEntity;
import com.zbkj.common.request.NovelHistoryRequest;
import com.zbkj.common.response.NovelHistoryResponse;
import com.zbkj.service.dao.NovelHistoryDao;
import com.zbkj.service.service.NovelHistoryService;
import com.zbkj.service.service.NovelRollChapterService;
import com.zbkj.service.service.UserService;
import org.hibernate.validator.constraints.Range;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;


/**
 * 小说历史记录 接口实现类
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * @author: 大粽子
 * +----------------------------------------------------------------------
 * @date Mon Apr 22 15:31:27 CST 2024
 * +----------------------------------------------------------------------
 * @email stivepeim@outlook.com
 * +----------------------------------------------------------------------
 */

@Service("novelHistoryService")
public class NovelHistoryServiceImpl extends ServiceImpl<NovelHistoryDao, NovelHistoryEntity> implements NovelHistoryService {

    @Resource
    NovelHistoryDao dao;

    @Autowired
    UserService userService;



    @Autowired
    NovelRollChapterService novelRollChapterService;
    @Override
    public String history(NovelHistoryRequest request) {

//        Integer userid= 2;
        Integer userid = userService.getUserId();
        if(userid == 0)
            throw new CrmebException("用户不存在或未登录");

        LambdaQueryWrapper<NovelRollChapterEntity> lqw = new LambdaQueryWrapper<>();
        lqw.eq(NovelRollChapterEntity::getChapterNum,request.getChapterIndex());
        lqw.eq(NovelRollChapterEntity::getNovelId,request.getBookId());
        NovelRollChapterEntity chapter = novelRollChapterService.getOne(lqw);
        if(ObjectUtil.isNull(chapter))
            throw new CrmebException("书或章节不存在");

        LambdaQueryWrapper<NovelHistoryEntity> lqw2 = new LambdaQueryWrapper<>();
        lqw2.eq(NovelHistoryEntity::getUId,userid);
        lqw2.eq(NovelHistoryEntity::getNovelId,request.getBookId());


        NovelHistoryEntity novelHistory = getOne(lqw2);
        novelHistory.setNovelId(request.getBookId());
        novelHistory.setChapterId(chapter.getId());
        novelHistory.setProgress(request.getProgress());
        novelHistory.setUId(userid);


        saveOrUpdate(novelHistory);
        return "历史记录保存成功";
    }
    @Override
    public NovelHistoryResponse getDetail(Integer bookId) {
        Integer userid = userService.getUserId();
        if(userid == 0)
            throw new CrmebException("用户不存在或未登录");

        LambdaQueryWrapper<NovelHistoryEntity> lqw = new LambdaQueryWrapper<>();
        lqw.eq(NovelHistoryEntity::getNovelId,bookId);
        lqw.eq(NovelHistoryEntity::getUId,userid);
        NovelHistoryEntity history =  getOne(lqw);
        if (ObjectUtil.isNull(history))
            throw new CrmebException("找不到该历史记录");

        NovelRollChapterEntity chapter = novelRollChapterService.getById(history.getId());
        if(ObjectUtil.isNull(chapter))
            throw new CrmebException("无法访问目标章节");


        NovelHistoryResponse response = new NovelHistoryResponse();
        response.setChapterIndex(chapter.getChapterNum());
        response.setProgress(history.getProgress());
        response.setBookId(bookId);
        return response;
    }
}
