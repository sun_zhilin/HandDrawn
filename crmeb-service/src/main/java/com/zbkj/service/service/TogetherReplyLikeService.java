package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.community.CommunityReplyLike;
import com.zbkj.common.model.together.TogetherReply;
import com.zbkj.common.model.together.TogetherReplyLike;

import java.util.List;

public interface TogetherReplyLikeService extends IService<TogetherReplyLike> {
    List<TogetherReplyLike> findListByNoteIdAndUid(Integer togetherId, Integer userId);

    TogetherReplyLike getDetail(Integer replyId, Integer userId);
}
