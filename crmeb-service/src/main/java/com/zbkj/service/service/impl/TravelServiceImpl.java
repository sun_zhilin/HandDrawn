package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.constants.ProductConstants;
import com.zbkj.common.model.community.CommunityNotes;
import com.zbkj.common.model.merchant.Merchant;
import com.zbkj.common.model.product.Product;
import com.zbkj.common.model.travel.Travel;
import com.zbkj.common.model.travel.TravelProduct;
import com.zbkj.common.model.travel.TravelTopic;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.TravelDetailRequest;
import com.zbkj.common.request.TravelShopRequest;
import com.zbkj.common.response.ProductFrontResponse;
import com.zbkj.common.response.ProductTagsFrontResponse;
import com.zbkj.common.response.TravelDetailResponse;
import com.zbkj.common.response.TravelListResponse;
import com.zbkj.common.util.BeanUtils;
import com.zbkj.service.dao.TravelDao;
import com.zbkj.service.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TravelServiceImpl extends ServiceImpl<TravelDao, Travel> implements TravelService {

    @Resource
    TravelDao dao;

    @Autowired
    ProductService productService;

    @Autowired
    private ProductReplyService productReplyService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    TravelTopicService travelTopicService;

    @Autowired
    TravelProductService travelProductService;

    @Autowired
    private ProductTagService productTagService;
    @Override
    public List<TravelListResponse> index() {
        LambdaQueryWrapper<Travel> lqw = new LambdaQueryWrapper<>();
        lqw.eq(Travel::getIsShow,1);
        lqw.eq(Travel::getIsDel,0);

        List<TravelListResponse> responseList = dao.selectList(lqw).stream().map(
                e->{
                    TravelListResponse response = new TravelListResponse();
                    BeanUtils.copyProperties(e,response);
                    return response;
                }
        ).collect(Collectors.toList());
        return responseList;
    }

    @Override
    public TravelDetailResponse detail(TravelDetailRequest request) {
        //查笔记
        TravelTopic travelTopic = travelTopicService.getById(request.getTravelId());

        //查旅游
        return null;
    }

    @Override
    public PageInfo<ProductFrontResponse> shop(TravelShopRequest request) {
        Page<ProductFrontResponse> page = PageHelper.startPage(request.getPage(), request.getLimit());
        LambdaQueryWrapper<TravelProduct> lqw = new LambdaQueryWrapper<>();
        lqw.eq(TravelProduct::getTravelId,request.getTravelId());
        List<TravelProduct> list = travelProductService.list(lqw);
        List<ProductFrontResponse> responseList = list.stream().map(e->{
            Product product = productService.getById(e.getProductId());
            ProductFrontResponse response = new ProductFrontResponse();
            BeanUtils.copyProperties(product,response);
            return response;
        }).collect(Collectors.toList());

        responseList.forEach(e -> {
            Merchant merchant = merchantService.getById(e.getMerId());
            e.setMerName(merchant.getName());
            e.setPhone(merchant.getPhone());
            // 评论总数
            Integer sumCount = productReplyService.getCountByScore(e.getId(), ProductConstants.PRODUCT_REPLY_TYPE_ALL);
            // 好评总数
            Integer goodCount = productReplyService.getCountByScore(e.getId(), ProductConstants.PRODUCT_REPLY_TYPE_GOOD);
            // 设置商品标签
            ProductTagsFrontResponse productTagsFrontResponse = productTagService.setProductTagByProductTagsRules(e.getId(), e.getBrandId(), e.getMerId(), e.getCategoryId(), e.getProductTags());
            e.setProductTags(productTagsFrontResponse);

            String replyChance = "0";
            if (sumCount > 0 && goodCount > 0) {
                replyChance = String.format("%.2f", ((goodCount.doubleValue() / sumCount.doubleValue())));
            }
            e.setReplyNum(sumCount);
            e.setPositiveRatio(replyChance);
            e.setSales(e.getSales() + e.getFicti());
        });
        return CommonPage.copyPageInfo(page, responseList);
    }
}
