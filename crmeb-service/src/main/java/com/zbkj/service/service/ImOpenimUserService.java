package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.community.CommunityNotes;
import com.zbkj.common.model.openim.ImOpenimUser;
import org.springframework.stereotype.Service;

@Service
public interface ImOpenimUserService extends IService<ImOpenimUser> {
}
