package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.ZoneUserTag;
import com.zbkj.common.model.wechat.WechatReply;
import com.zbkj.service.dao.WechatReplyDao;
import com.zbkj.service.dao.ZoneUserTagDao;
import com.zbkj.service.service.WechatReplyService;
import com.zbkj.service.service.ZoneUserTagService;
import org.springframework.stereotype.Service;

@Service
public class ZoneUserTagServiceImpl extends ServiceImpl<ZoneUserTagDao, ZoneUserTag> implements ZoneUserTagService {
}
