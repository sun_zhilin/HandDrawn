package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.together.TogetherJobRecord;
import com.zbkj.common.model.together.TogetherProductOrder;

public interface TogetherJobRecordService extends IService<TogetherJobRecord> {
}
