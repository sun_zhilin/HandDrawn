package com.zbkj.service.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.model.communityOrder.CommunityOrder;
import com.zbkj.common.request.PageParamRequest;

public interface CommunityOrderService  extends IService<CommunityOrder> {

    /**
     * 笔记专题列表
     * @param pageParamRequest 分页参数
     * @return PageInfo
     */
    PageInfo<CommunityOrder> getList(PageParamRequest pageParamRequest,CommunityOrder communityOrder);
}
