package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.travel.TravelApply;

public interface TravelApplyService extends IService<TravelApply> {
}
