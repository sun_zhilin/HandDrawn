package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.novel.NovelAuthoritytEntity;

import java.util.List;

public interface NovelAuthorityService extends IService<NovelAuthoritytEntity> {
    List<NovelAuthoritytEntity> getAllByNovelId(Integer novelId);
}
