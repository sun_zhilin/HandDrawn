package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.together.UserTogether;
import com.zbkj.common.model.together.UserTogetherRecord;
import com.zbkj.service.dao.UserTogetherDao;
import com.zbkj.service.dao.UserTogetherRecordDao;
import com.zbkj.service.service.UserTogetherRecordService;
import com.zbkj.service.service.UserTogetherService;
import org.springframework.stereotype.Service;

@Service
public class UserTogetherRecordServiceImpl extends ServiceImpl<UserTogetherRecordDao, UserTogetherRecord> implements UserTogetherRecordService {
}
