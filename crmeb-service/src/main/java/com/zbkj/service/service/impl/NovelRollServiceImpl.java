package com.zbkj.service.service.impl;

import com.zbkj.common.model.novel.NovelRollEntity;
import com.zbkj.service.dao.NovelRollDao;
import com.zbkj.service.service.NovelRollService;
import org.springframework.stereotype.Service;
import java.util.Map;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;



/**
 * 小说卷表 接口实现类
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * @author: 大粽子
 * +----------------------------------------------------------------------
 * @date Mon Apr 22 15:31:27 CST 2024
 * +----------------------------------------------------------------------
 * @email stivepeim@outlook.com
 * +----------------------------------------------------------------------
 */

@Service("novelRollService")
public class NovelRollServiceImpl extends ServiceImpl<NovelRollDao, NovelRollEntity> implements NovelRollService {

    /**
     * NovelRoll列表查询
     * @param request 默认是是体类 根据自己需求修改或者创建自己的request
     * @param pageParamRequest 分页参数对象
     * @return
     */
//    @Override
//    public PageUtils queryPage(NovelRollEntity request, PageParamRequest pageParamRequest) {
//        PageHelper.startPage(pageParamRequest.getPageNum(), pageParamRequest.getPageSize());
//
//        //列表查询 NovelRoll 类的多条件查询
//        LambdaQueryWrapper<NovelRoll> lambdaQueryWrapper = new LambdaQueryWrapper<>();
//        NovelRoll model = new NovelRoll();
//        BeanUtils.copyProperties(request, model);
//        lambdaQueryWrapper.setEntity(model);
//        return dao.selectList(lambdaQueryWrapper);
//    }

}
