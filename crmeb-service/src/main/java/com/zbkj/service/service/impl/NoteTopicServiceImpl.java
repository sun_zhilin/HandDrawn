package com.zbkj.service.service.impl;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.exception.CrmebException;
import com.zbkj.common.model.community.CommunityNotes;
import com.zbkj.common.model.communityOrder.NoteTopic;
import com.zbkj.common.model.communityOrder.UserNote;
import com.zbkj.common.model.user.User;
import com.zbkj.common.model.user.UserTag;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.NoteTopicRequest;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.utils.CrmebDateUtil;
import com.zbkj.service.dao.NoteTopicDao;
import com.zbkj.service.dao.UserTagDao;
import com.zbkj.service.service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;

@Service
public class NoteTopicServiceImpl extends ServiceImpl<NoteTopicDao, NoteTopic> implements NoteTopicService {
    @Resource
    private NoteTopicDao dao;

    @Autowired
    private NoteTopicService noteTopicService;

    @Autowired
    private CommunityNotesService communityNotesService;

    private static final Logger logger = LoggerFactory.getLogger(NoteTopicService.class);

    @Override
    public PageInfo<NoteTopic> getList(PageParamRequest pageParamRequest) {
        Page<Object> page = PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());
        LambdaQueryWrapper<NoteTopic> lqw = Wrappers.lambdaQuery();
        lqw.orderByDesc(NoteTopic::getId);
        List<NoteTopic> tagList = dao.selectList(lqw);
        return CommonPage.copyPageInfo(page, tagList);
    }

    @Override
    public Boolean create(NoteTopicRequest noteTopicRequest) {
        NoteTopic noteTopic = new NoteTopic();
        BeanUtils.copyProperties(noteTopicRequest, noteTopic);
        noteTopic.setCreateTime(CrmebDateUtil.nowDateTime());
        noteTopic.setId(null);
        return save(noteTopic);
    }

    @Override
    public Boolean delete(Integer id) {
        NoteTopic noteTopic = getById(id);
        if (ObjectUtil.isNull(noteTopic)) {
            return Boolean.TRUE;
        }
        // 判断是否有用户使用笔记使用此专题
        if (isNote(id)) {
            throw new CrmebException("笔记使用此专题，无法删除");
        }
        return removeById(id);
    }

    @Override
    public Boolean updateTag(NoteTopicRequest noteTopicRequest) {
        if (ObjectUtil.isNull(noteTopicRequest.getId())) {
            throw new CrmebException("ID不能为空");
        }
        NoteTopic userTag = getById(noteTopicRequest.getId());
        userTag.setTopicName(noteTopicRequest.getTopicName());
        userTag.setPrice(noteTopicRequest.getPrice());
        userTag.setIsFree(noteTopicRequest.getIsFree());
        return updateById(userTag);
    }

    @Override
    public List<NoteTopic> getAllList() {
        LambdaQueryWrapper<NoteTopic> lqw = Wrappers.lambdaQuery();
        lqw.orderByDesc(NoteTopic::getId);
        return dao.selectList(lqw);
    }

    public Boolean isNote(Integer topicId) {
        LambdaQueryWrapper<CommunityNotes> lqw = Wrappers.lambdaQuery();
        lqw.select(CommunityNotes::getId);
        lqw.apply(StrUtil.format("find_in_set('{}', topic_id)", topicId));
        lqw.last(" limit 1");
        CommunityNotes user = communityNotesService.getOne(lqw);
        return ObjectUtil.isNotNull(user);
    }
}
