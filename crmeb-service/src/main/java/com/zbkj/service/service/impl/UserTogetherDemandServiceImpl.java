package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.together.UserTogetherDemand;
import com.zbkj.service.dao.UserTogetherDemandDao;
import com.zbkj.service.service.UserTogetherDemandService;
import org.springframework.stereotype.Service;

@Service
public class UserTogetherDemandServiceImpl extends ServiceImpl<UserTogetherDemandDao, UserTogetherDemand> implements UserTogetherDemandService {
}
