package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.novel.NovelEntity;
import com.zbkj.common.model.novel.NovelRollChapterEntity;
import com.zbkj.common.request.NovelChapterUpdateRequest;
import com.zbkj.common.response.NovelContentResponse;
import com.zbkj.common.vo.MyRecord;
//import com.zbkj.common.utils.PageUtils;

import java.util.List;
import java.util.Map;

/**
 * 小说章节表 业务接口
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * @author: 大粽子
 * +----------------------------------------------------------------------
 * @date Mon Apr 22 15:31:27 CST 2024
 * +----------------------------------------------------------------------
 * @email stivepeim@outlook.com
 * +----------------------------------------------------------------------
 */
public interface NovelRollChapterService extends IService<NovelRollChapterEntity> {

    List<NovelRollChapterEntity> getChaptersByNovelId(Integer id);

    List<NovelRollChapterEntity> getlastZJById(Integer id);

    NovelContentResponse getContent(Integer id);

//     chapterUpdate( );

    void chapterUpdate(NovelChapterUpdateRequest request);
//    List<NovelRollChapter> getList(NovelRollChapterEntity request, PageParamRequest pageParamRequest)

    String chapterOpen(Integer id);

    String chapterunrelease(Integer id);

    String delete(Integer id);

    String reduce(Integer id);
}

