package com.zbkj.service.service.impl;

import java.util.List;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.notice.NoticeType;
import com.zbkj.service.dao.NoticeTypeMapper;
import com.zbkj.service.service.INoticeTypeService;
import org.springframework.stereotype.Service;
/**
 * 【请填写功能名称】Service业务层处理
 *
 * @author my
 * @date 2024-04-07
 */
@Service
public class NoticeTypeServiceImpl extends ServiceImpl<NoticeTypeMapper, NoticeType> implements INoticeTypeService
{
}
