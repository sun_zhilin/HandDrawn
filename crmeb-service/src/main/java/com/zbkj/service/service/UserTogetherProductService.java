package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.together.UserTogether;
import com.zbkj.common.model.together.UserTogetherProduct;

public interface UserTogetherProductService extends IService<UserTogetherProduct> {
}
