package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.ZoneUserTag;

public interface ZoneUserTagService extends IService<ZoneUserTag> {

}
