package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.together.TogetherProductOrder;
import com.zbkj.service.dao.TogetherProductOrderDao;
import com.zbkj.service.service.TogetherProductOrderService;
import org.springframework.stereotype.Service;

@Service
public class TogetherProductOrderServiceImpl extends ServiceImpl<TogetherProductOrderDao, TogetherProductOrder> implements TogetherProductOrderService {

}
