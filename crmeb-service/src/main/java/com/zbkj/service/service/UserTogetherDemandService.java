package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.together.UserTogetherDemand;
import com.zbkj.common.model.user.UserTag;

public interface UserTogetherDemandService extends IService<UserTogetherDemand> {
}
