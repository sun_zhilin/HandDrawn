package com.zbkj.service.service.impl;

import com.zbkj.common.model.novel.NovelCategoryEntity;
import com.zbkj.common.response.NovelCategoryResponse;
import com.zbkj.service.dao.NovelCategoryDao;
import com.zbkj.service.service.NovelCategoryService;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import javax.annotation.Resource;


/**
 * 小说分类表 接口实现类
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * @author: 大粽子
 * +----------------------------------------------------------------------
 * @date Mon Apr 22 15:31:27 CST 2024
 * +----------------------------------------------------------------------
 * @email stivepeim@outlook.com
 * +----------------------------------------------------------------------
 */

@Service("novelCategoryService")
public class NovelCategoryServiceImpl extends ServiceImpl<NovelCategoryDao, NovelCategoryEntity> implements NovelCategoryService {

    @Resource
    NovelCategoryDao novelCategoryDao;

    @Override
    public List<NovelCategoryResponse> index() {
        List<NovelCategoryEntity> list =  novelCategoryDao.index();
        List<NovelCategoryResponse> responseList = new ArrayList<>();
        list.forEach(e->{
            NovelCategoryResponse response = new NovelCategoryResponse();
            response.setId(e.getId());
            response.setName(e.getName());
            responseList.add(response);
        });
        return responseList;
    }

    @Override
    public String getNameById(Integer id) {

        return novelCategoryDao.getNameById(id);
    }
//    public PageUtils queryPage(NovelCategoryEntity request, PageParamRequest pageParamRequest) {
//        PageHelper.startPage(pageParamRequest.getPageNum(), pageParamRequest.getPageSize());
//
//        //列表查询 NovelCategory 类的多条件查询
//        LambdaQueryWrapper<NovelCategory> lambdaQueryWrapper = new LambdaQueryWrapper<>();
//        NovelCategory model = new NovelCategory();
//        BeanUtils.copyProperties(request, model);
//        lambdaQueryWrapper.setEntity(model);
//        return dao.selectList(lambdaQueryWrapper);
//    }

}
