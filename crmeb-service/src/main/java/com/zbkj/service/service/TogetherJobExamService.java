package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.together.TogetherJobExam;
import com.zbkj.common.vo.MyRecord;

import java.util.List;

public interface TogetherJobExamService extends IService<TogetherJobExam> {

    TogetherJobExam getByDUid (Integer uid1,Integer uid2);

    String handle(TogetherJobExam request);
}
