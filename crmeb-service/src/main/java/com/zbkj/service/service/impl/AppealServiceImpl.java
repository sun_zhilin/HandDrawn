package com.zbkj.service.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.zbkj.common.model.Appeal;
import com.zbkj.service.dao.AppealDao;
import com.zbkj.service.service.AppealService;
import org.springframework.stereotype.Service;

@Service
public class AppealServiceImpl extends ServiceImpl<AppealDao, Appeal> implements AppealService {
}
