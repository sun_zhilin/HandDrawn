package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.request.*;
import com.zbkj.common.response.CommunityNoteCollectionRespone;
import com.zbkj.service.entity.EbUserNoteCollection;

import java.util.List;

/**
 * 用户收藏note(EbUserNoteCollection)表服务接口
 *
 * @author makejava
 * @since 2024-03-19 16:04:41
 */
public interface EbUserNoteCollectionService extends IService<EbUserNoteCollection> {

    Boolean add(NoteCollectRequest request);


    Boolean delete(CancelNoteCollectRequest request);

    PageInfo<CommunityNoteCollectionRespone> getUserList(PageParamRequest pageParamRequest);
}

