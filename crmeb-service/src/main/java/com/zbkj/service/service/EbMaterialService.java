package com.zbkj.service.service;

import com.zbkj.common.vo.FileResultVo;
import com.zbkj.service.entity.EbMaterial;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.multipart.MultipartFile;

import java.util.List;

/**
 * 素材附件(EbMaterial)表服务接口
 *
 * @author makejava
 * @since 2024-03-19 11:04:44
 */
public interface EbMaterialService {

    /**
     * 通过ID查询单条数据
     *
     * @param id 主键
     * @return 实例对象
     */
    EbMaterial queryById(Integer id);
    EbMaterial queryByProductId(Integer id);

    /**
     * 分页查询
     *
     * @param ebMaterial 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
    Page<EbMaterial> queryByPage(EbMaterial ebMaterial, PageRequest pageRequest);

    /**
     * 新增数据
     *
     * @param ebMaterial 实例对象
     * @return 实例对象
     */
    EbMaterial insert(EbMaterial ebMaterial);

    /**
     * 修改数据
     *
     * @param ebMaterial 实例对象
     * @return 实例对象
     */
    EbMaterial update(EbMaterial ebMaterial);

    /**
     * 通过主键删除数据
     *
     * @param id 主键
     * @return 是否成功
     */
    boolean deleteById(Integer id);


    List<FileResultVo> download(String preOrderNo);
}
