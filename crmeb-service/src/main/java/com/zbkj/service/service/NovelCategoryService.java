package com.zbkj.service.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.zbkj.common.model.novel.NovelCategoryEntity;


import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.response.NovelCategoryResponse;

import java.util.List;
import java.util.Map;

/**
 * 小说分类表 业务接口
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * @author: 大粽子
 * +----------------------------------------------------------------------
 * @date Mon Apr 22 15:31:27 CST 2024
 * +----------------------------------------------------------------------
 * @email stivepeim@outlook.com
 * +----------------------------------------------------------------------
 */
public interface NovelCategoryService extends IService<NovelCategoryEntity> {

//    List<NovelCategory> getList(NovelCategoryEntity request, PageParamRequest pageParamRequest);

    String getNameById(Integer id);

    List<NovelCategoryResponse> index();
}

