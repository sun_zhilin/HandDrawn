package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.record.TradingDayRecord;
import com.zbkj.common.model.together.TogetherProductOrder;

public interface TogetherProductOrderDao extends BaseMapper<TogetherProductOrder> {
}
