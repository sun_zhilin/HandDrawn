package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.together.TogetherReply;

public interface TogetherReplyDao extends BaseMapper<TogetherReply> {
}
