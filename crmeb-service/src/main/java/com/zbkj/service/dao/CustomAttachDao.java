package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.custom.CustomAttach;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

public interface CustomAttachDao extends BaseMapper<CustomAttach> {

    @Select("SELECT * FROM tb_custom_attach WHERE custom_id = #{id} AND type = #{type}")
    List<CustomAttach> showFile(@Param("id") Integer id, @Param("type") Integer type);
}
