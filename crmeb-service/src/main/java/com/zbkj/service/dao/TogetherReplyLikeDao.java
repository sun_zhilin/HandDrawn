package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.together.TogetherReplyLike;

public interface TogetherReplyLikeDao extends BaseMapper<TogetherReplyLike> {
}
