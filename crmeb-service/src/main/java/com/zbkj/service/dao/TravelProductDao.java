package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.travel.Travel;
import com.zbkj.common.model.travel.TravelProduct;

public interface TravelProductDao  extends BaseMapper<TravelProduct> {
}
