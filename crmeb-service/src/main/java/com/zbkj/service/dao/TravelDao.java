package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.travel.Travel;

public interface TravelDao extends BaseMapper<Travel> {
}
