package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import com.zbkj.common.model.openim.ImOpenimUser;

public interface ImOpenimUserDao extends BaseMapper<ImOpenimUser> {
}
