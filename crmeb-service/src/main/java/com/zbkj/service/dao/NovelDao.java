package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.novel.NovelEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * 小说对象表 DAO 映射层
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * @author: 大粽子
 * +----------------------------------------------------------------------
 * @date Mon Apr 22 15:31:27 CST 2024
 * +----------------------------------------------------------------------
 * @email stivepeim@outlook.com
 * +----------------------------------------------------------------------
 */
@Mapper
public interface NovelDao extends BaseMapper<NovelEntity> {

    @Select("SELECT t1.* FROM tb_novel t1 JOIN eb_user t2 ON t1.u_id = t2.id " +
            "WHERE t1.is_del = 0 AND t1.is_show = 1 AND (t1.articlename LIKE CONCAT('%', #{keyword}, '%') " +
            "OR t1.intro LIKE CONCAT('%', #{keyword}, '%') " +
            "OR t2.nickname LIKE CONCAT('%', #{keyword}, '%'))" +
            "ORDER BY t1.update_time DESC")
    List<NovelEntity> findByKeyword(String keyword);

}
