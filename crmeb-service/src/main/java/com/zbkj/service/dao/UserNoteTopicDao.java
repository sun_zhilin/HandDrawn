package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.communityOrder.NoteTopic;
import com.zbkj.common.model.communityOrder.UserNoteTopic;

public interface UserNoteTopicDao extends BaseMapper<UserNoteTopic> {
}
