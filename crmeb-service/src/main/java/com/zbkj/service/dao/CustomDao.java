package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.custom.Custom;

public interface CustomDao extends BaseMapper<Custom> {
}
