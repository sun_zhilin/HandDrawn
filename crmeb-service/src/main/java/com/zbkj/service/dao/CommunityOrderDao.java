package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.communityOrder.CommunityOrder;


public interface CommunityOrderDao extends BaseMapper<CommunityOrder> {


}
