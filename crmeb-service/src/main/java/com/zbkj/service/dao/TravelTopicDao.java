package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.travel.TravelTopic;

public interface TravelTopicDao extends BaseMapper<TravelTopic> {
}
