package com.zbkj.service.dao;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.together.TogetherJob;

public interface TogetherJobDao  extends BaseMapper<TogetherJob> {
}
