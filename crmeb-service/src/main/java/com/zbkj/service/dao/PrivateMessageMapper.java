package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.PrivateMessage;


public interface PrivateMessageMapper extends BaseMapper<PrivateMessage> {

}
