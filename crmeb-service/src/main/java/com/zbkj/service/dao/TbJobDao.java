package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.together.TbJob;

public interface TbJobDao extends BaseMapper<TbJob> {
}
