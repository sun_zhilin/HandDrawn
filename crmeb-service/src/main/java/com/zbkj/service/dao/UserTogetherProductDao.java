package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.together.TbJob;
import com.zbkj.common.model.together.UserTogetherProduct;

public interface UserTogetherProductDao extends BaseMapper<UserTogetherProduct> {
}
