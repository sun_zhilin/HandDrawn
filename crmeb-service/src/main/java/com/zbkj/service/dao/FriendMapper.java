package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.Friend;


public interface FriendMapper extends BaseMapper<Friend> {

}
