package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.together.UserTogetherProduct;
import com.zbkj.common.model.together.UserTogetherRecord;

public interface UserTogetherRecordDao extends BaseMapper<UserTogetherRecord> {
}
