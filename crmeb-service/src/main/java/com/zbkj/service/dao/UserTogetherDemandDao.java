package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.coupon.Coupon;
import com.zbkj.common.model.together.UserTogetherDemand;

public interface UserTogetherDemandDao extends BaseMapper<UserTogetherDemand> {

}
