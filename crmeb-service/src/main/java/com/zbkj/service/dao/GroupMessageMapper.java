package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.GroupMessage;


public interface GroupMessageMapper extends BaseMapper<GroupMessage> {

}
