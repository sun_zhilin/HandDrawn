package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.response.CommunityNoteCollectionRespone;
import com.zbkj.common.response.UserProductRelationResponse;
import com.zbkj.service.entity.EbUserNoteCollection;

import java.util.List;

/**
 * 用户收藏note(EbUserNoteCollection)表数据库访问层
 *
 * @author makejava
 * @since 2024-03-19 16:04:41
 */
public interface EbUserNoteCollectionDao extends BaseMapper<EbUserNoteCollection> {

    List<CommunityNoteCollectionRespone> getUserList(Integer uid);
}

