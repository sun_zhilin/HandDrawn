package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.together.UserTogetherAgree;

public interface UserTogetherAgreeDao  extends BaseMapper<UserTogetherAgree> {
}
