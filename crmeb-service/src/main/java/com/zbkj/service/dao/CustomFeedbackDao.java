package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.custom.CustomFeedback;

public interface CustomFeedbackDao extends BaseMapper<CustomFeedback> {
}
