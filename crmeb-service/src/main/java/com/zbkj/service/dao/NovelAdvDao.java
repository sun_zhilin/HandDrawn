package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.novel.NovelAdv;

public interface NovelAdvDao extends BaseMapper<NovelAdv> {

}
