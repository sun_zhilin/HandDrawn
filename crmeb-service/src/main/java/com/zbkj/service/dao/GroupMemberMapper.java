package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.GroupMember;

public interface GroupMemberMapper extends BaseMapper<GroupMember> {

}
