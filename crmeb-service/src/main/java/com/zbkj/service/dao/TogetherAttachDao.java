package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.together.TogetherAttach;

public interface TogetherAttachDao extends BaseMapper<TogetherAttach> {
}
