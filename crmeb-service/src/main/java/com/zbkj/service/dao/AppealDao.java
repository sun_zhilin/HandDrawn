package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.Appeal;

public interface AppealDao extends BaseMapper<Appeal> {
}
