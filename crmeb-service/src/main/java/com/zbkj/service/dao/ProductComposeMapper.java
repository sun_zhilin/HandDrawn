package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.product.ProductCompose;

public interface ProductComposeMapper extends BaseMapper<ProductCompose> {
}
