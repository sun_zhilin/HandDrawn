package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.Group;


public interface GroupMapper extends BaseMapper<Group> {

}
