package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.together.TogetherJobExam;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

public interface TogetherJobExamDao extends BaseMapper<TogetherJobExam> {

    @Select("SELECT * FROM tb_together_job_exam e WHERE " +
            "(e.uid = #{id1} AND e.suid = #{id2}) " +
            " AND e.is_del = 0" +
            " AND e.status < 3" +
            " OR " +
            "(e.uid = #{id2} AND e.suid = #{id1})" +
            " AND e.is_del = 0" +
            " AND e.status < 3")
    TogetherJobExam getByDUid (@Param("id1")Integer uid1, @Param("id2")Integer uid2);
}
