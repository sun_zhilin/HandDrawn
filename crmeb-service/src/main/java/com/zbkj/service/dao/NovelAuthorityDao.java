package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.novel.NovelAuthoritytEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface NovelAuthorityDao extends BaseMapper<NovelAuthoritytEntity> {

    @Select("SELECT * FROM tb_novel_authority WHERE novel_id = #{novelId}")
    List<NovelAuthoritytEntity> getAllByNovelId(@Param("novelId") Integer novelId);
}
