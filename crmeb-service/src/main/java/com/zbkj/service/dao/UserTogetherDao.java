package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.together.UserTogether;

public interface UserTogetherDao extends BaseMapper<UserTogether> {
}
