package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.user.UserJob;

public interface UserJobDao extends BaseMapper<UserJob> {
}
