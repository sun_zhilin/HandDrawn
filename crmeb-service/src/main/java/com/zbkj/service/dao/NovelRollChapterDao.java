package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.novel.NovelRollChapterEntity;
import com.zbkj.common.response.NovelContentResponse;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

/**
 * 小说章节表 DAO 映射层
 * +----------------------------------------------------------------------
 * | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
 * +----------------------------------------------------------------------
 * | Copyright (c) 2016~2022 https://www.crmeb.com All rights reserved.
 * +----------------------------------------------------------------------
 * | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
 * +----------------------------------------------------------------------
 * @author: 大粽子
 * +----------------------------------------------------------------------
 * @date Mon Apr 22 15:31:27 CST 2024
 * +----------------------------------------------------------------------
 * @email stivepeim@outlook.com
 * +----------------------------------------------------------------------
 */
@Mapper
public interface NovelRollChapterDao extends BaseMapper<NovelRollChapterEntity> {

    @Select("SELECT id,is_show,is_del,create_time,update_time,chapter_name,chapter_num,novel_id,roll_id FROM tb_novel_roll_chapter " +
            "        WHERE novel_id = #{id} " +
            "        ORDER BY create_time ASC")
    List<NovelRollChapterEntity> getChaptersByNovelId(@Param("id")Integer id);

    @Select("SELECT id,is_show,is_del,create_time,update_time,chapter_name,chapter_num,novel_id,roll_id FROM tb_novel_roll_chapter " +
            "        WHERE novel_id = #{id}" +
            "        AND is_open = 1 " +
            "        ORDER BY create_time DESC " +
            "        LIMIT 12")
    List<NovelRollChapterEntity> getlastZJById(@Param("id")Integer id);


    @Select("SELECT content FROM tb_novel_roll_chapter where id = #{id}")
    NovelRollChapterEntity getContent(@Param("id") Integer id);
}
