package com.zbkj.service.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.zbkj.common.model.notice.NoticeType;

import java.util.List;

/**
 * 【请填写功能名称】Mapper接口
 *
 * @author my
 * @date 2024-04-07
 */
public interface NoticeTypeMapper extends BaseMapper<NoticeType>
{
}
