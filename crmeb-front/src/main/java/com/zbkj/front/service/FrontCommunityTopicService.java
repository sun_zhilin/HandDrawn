package com.zbkj.front.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.model.community.CommunityTopic;
import com.zbkj.common.request.CommonSearchRequest;
import com.zbkj.common.request.CommunityTopicSaveRequest;
import com.zbkj.common.request.CommunityTopicSearchRequest;

import java.util.List;
import java.util.Map;

/**
* CommunityTopic 接口
* +----------------------------------------------------------------------
* | CRMEB [ CRMEB赋能开发者，助力企业发展 ]
* +----------------------------------------------------------------------
* | Copyright (c) 2016~2023 https://www.crmeb.com All rights reserved.
* +----------------------------------------------------------------------
* | Licensed CRMEB并不是自由软件，未经许可不能去掉CRMEB相关版权
* +----------------------------------------------------------------------
* | Author: CRMEB Team <admin@crmeb.com>
* +----------------------------------------------------------------------
*/
public interface FrontCommunityTopicService extends IService<CommunityTopic> {

    /**
     * 添加社区话题
     */
    void add(CommunityTopicSaveRequest request);
}