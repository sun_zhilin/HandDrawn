package com.zbkj.front.controller;


import com.zbkj.common.model.user.User;
import com.zbkj.common.result.Result;
import com.zbkj.common.result.ResultUtils;
import com.zbkj.common.session.UserSession;
import com.zbkj.common.vo.GroupInviteVO;
import com.zbkj.common.vo.GroupMemberVO;
import com.zbkj.common.vo.GroupVO;
import com.zbkj.service.service.IGroupService;
import com.zbkj.service.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Api(tags = "群聊")
@RestController
@RequestMapping("api/front/group")
@RequiredArgsConstructor
public class GroupController {

    private final IGroupService groupService;

    @Autowired
    private UserService userService;

    @ApiOperation(value = "创建群聊", notes = "创建群聊")
    @PostMapping("/create")
    public Result<GroupVO> createGroup(@Valid @RequestBody GroupVO vo,HttpServletRequest request) {
        User info = userService.getInfo();
        UserSession userSession = new UserSession();
        userSession.setNickName(info.getNickname());
        userSession.setUserName(info.getAccount());
        userSession.setUserId(info.getId().longValue());
        userSession.setTerminal(0);
        request.setAttribute("session", userSession);
        return ResultUtils.success(groupService.createGroup(vo));
    }

    @ApiOperation(value = "修改群聊信息", notes = "修改群聊信息")
    @PutMapping("/modify")
    public Result<GroupVO> modifyGroup(@Valid @RequestBody GroupVO vo,HttpServletRequest request) {
        User info = userService.getInfo();
        UserSession userSession = new UserSession();
        userSession.setNickName(info.getNickname());
        userSession.setUserName(info.getAccount());
        userSession.setUserId(info.getId().longValue());
        userSession.setTerminal(0);
        request.setAttribute("session", userSession);
        return ResultUtils.success(groupService.modifyGroup(vo));
    }

    @ApiOperation(value = "解散群聊", notes = "解散群聊")
    @DeleteMapping("/delete/{groupId}")
    public Result deleteGroup(@NotNull(message = "群聊id不能为空") @PathVariable Long groupId,HttpServletRequest request) {
        User info = userService.getInfo();
        UserSession userSession = new UserSession();
        userSession.setNickName(info.getNickname());
        userSession.setUserName(info.getAccount());
        userSession.setUserId(info.getId().longValue());
        userSession.setTerminal(0);
        request.setAttribute("session", userSession);
        groupService.deleteGroup(groupId);
        return ResultUtils.success();
    }

    @ApiOperation(value = "查询群聊", notes = "查询单个群聊信息")
    @GetMapping("/find/{groupId}")
    public Result<GroupVO> findGroup(@NotNull(message = "群聊id不能为空") @PathVariable Long groupId,HttpServletRequest request) {
        User info = userService.getInfo();
        UserSession userSession = new UserSession();
        userSession.setNickName(info.getNickname());
        userSession.setUserName(info.getAccount());
        userSession.setUserId(info.getId().longValue());
        userSession.setTerminal(0);
        request.setAttribute("session", userSession);
        return ResultUtils.success(groupService.findById(groupId));
    }

    @ApiOperation(value = "查询群聊列表", notes = "查询群聊列表")
    @GetMapping("/list")
    public Result<List<GroupVO>> findGroups(HttpServletRequest request) {
        User info = userService.getInfo();
        UserSession userSession = new UserSession();
        userSession.setNickName(info.getNickname());
        userSession.setUserName(info.getAccount());
        userSession.setUserId(info.getId().longValue());
        userSession.setTerminal(0);
        request.setAttribute("session", userSession);
        return ResultUtils.success(groupService.findGroups());
    }

    @ApiOperation(value = "邀请进群", notes = "邀请好友进群")
    @PostMapping("/invite")
    public Result invite(@Valid @RequestBody GroupInviteVO vo,HttpServletRequest request) {
        User info = userService.getInfo();
        UserSession userSession = new UserSession();
        userSession.setNickName(info.getNickname());
        userSession.setUserName(info.getAccount());
        userSession.setUserId(info.getId().longValue());
        userSession.setTerminal(0);
        request.setAttribute("session", userSession);

        groupService.invite(vo);
        return ResultUtils.success();
    }

    @ApiOperation(value = "查询群聊成员", notes = "查询群聊成员")
    @GetMapping("/members/{groupId}")
    public Result<List<GroupMemberVO>> findGroupMembers(@NotNull(message = "群聊id不能为空") @PathVariable Long groupId,HttpServletRequest request) {
        User info = userService.getInfo();
        UserSession userSession = new UserSession();
        userSession.setNickName(info.getNickname());
        userSession.setUserName(info.getAccount());
        userSession.setUserId(info.getId().longValue());
        userSession.setTerminal(0);
        request.setAttribute("session", userSession);
        return ResultUtils.success(groupService.findGroupMembers(groupId));
    }

    @ApiOperation(value = "退出群聊", notes = "退出群聊")
    @DeleteMapping("/quit/{groupId}")
    public Result quitGroup(@NotNull(message = "群聊id不能为空") @PathVariable Long groupId,HttpServletRequest request) {
        User info = userService.getInfo();
        UserSession userSession = new UserSession();
        userSession.setNickName(info.getNickname());
        userSession.setUserName(info.getAccount());
        userSession.setUserId(info.getId().longValue());
        userSession.setTerminal(0);
        request.setAttribute("session", userSession);
        groupService.quitGroup(groupId);
        return ResultUtils.success();
    }

    @ApiOperation(value = "踢出群聊", notes = "将用户踢出群聊")
    @DeleteMapping("/kick/{groupId}")
    public Result kickGroup(@NotNull(message = "群聊id不能为空") @PathVariable Long groupId,
                            @NotNull(message = "用户id不能为空") @RequestParam Long userId,HttpServletRequest request) {
        User info = userService.getInfo();
        UserSession userSession = new UserSession();
        userSession.setNickName(info.getNickname());
        userSession.setUserName(info.getAccount());
        userSession.setUserId(info.getId().longValue());
        userSession.setTerminal(0);
        request.setAttribute("session", userSession);
        groupService.kickGroup(groupId, userId);
        return ResultUtils.success();
    }

}

