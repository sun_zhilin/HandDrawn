package com.zbkj.front.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.exception.CrmebException;
import com.zbkj.common.model.merchant.Merchant;
import com.zbkj.common.model.product.Product;
import com.zbkj.common.model.together.TogetherJob;
import com.zbkj.common.model.together.UserTogether;
import com.zbkj.common.model.user.User;
import com.zbkj.common.model.user.UserJob;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.PersonJobRequest;
import com.zbkj.common.request.ProductFrontSearchRequest;
import com.zbkj.common.response.ProductDetailResponse;
import com.zbkj.common.response.ProductFrontResponse;
import com.zbkj.common.result.CommonResult;
import com.zbkj.front.service.FrontProductService;
import com.zbkj.service.service.*;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("api/front/home")
public class HomoController {
    @Autowired
    TogetherJobService togetherJobService;

    @Autowired
    UserTogetherService userTogetherService;
    @Autowired
    private UserJobService userJobService;
    @Autowired
    private UserService userService;

    @Autowired
    private MerchantService merchantService;

    @Autowired
    private FrontProductService frontProductService;

    @ApiOperation(value = "主页共创列表")
    @RequestMapping(value = "/together/list", method = RequestMethod.GET)
    public CommonPage<UserTogether> ListTogether(@ModelAttribute @Validated ProductFrontSearchRequest request,
                                                 @ModelAttribute @Validated PageParamRequest pageParamRequest) {
        LambdaQueryWrapper<TogetherJob> lqw = new LambdaQueryWrapper<>();

        lqw.eq(TogetherJob::getUid, request.getUserId());
        Page<TogetherJob> page = PageHelper.startPage(pageParamRequest.getPage(), pageParamRequest.getLimit());
        List<TogetherJob> list = togetherJobService.list(lqw);


        List<UserTogether> responselist = list.stream().map(
                e -> {
                    UserTogether userTogether = userTogetherService.getById(e.getTogetherId());
//                    e.setUserTogether(userTogether);
                    return userTogether;
                }
        ).collect(Collectors.toList());

        return CommonPage.restPage(CommonPage.copyPageInfo(page, responselist));
    }

    @ApiOperation(value = "根据角色列用户")
    @RequestMapping(value = "/getByJob", method = RequestMethod.POST)
    public CommonResult<List<User>> getByJob(@RequestBody PersonJobRequest request) {
        Page<User> page = PageHelper.startPage(request.getPage(), request.getLimit());

        LambdaQueryWrapper<User> lqw2 = new LambdaQueryWrapper<>();
        lqw2.apply("FIND_IN_SET(" + request.getId() + ", tag_id) > 0");
        List<User> list1 = userService.list(lqw2);


//        LambdaQueryWrapper<UserJob> lqw  = new LambdaQueryWrapper<>();
//        lqw.eq(UserJob::getJobId,request.getId());
//        List<UserJob> list1 = userJobService.list(lqw);
//
//
//
//        List<User> list = list1.stream().map(
//                e->{
////                    if()
//                    User response = userService.getById(e.getUid());
//                    return response;
//                }
//        ).collect(Collectors.toList());

        return CommonResult.success(list1);
    }

    @ApiOperation(value = "我的主页中的商品分页列表")
    @RequestMapping(value = "/homepage/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<ProductFrontResponse>> getHomepageList(@ModelAttribute @Validated ProductFrontSearchRequest request,
                                                                          @ModelAttribute @Validated PageParamRequest pageParamRequest
    ) {

        if (request.getUserId() == null) {
            throw new CrmebException("用户id不能为空");
        }
        if (request.getType() == null) {
            throw new CrmebException("类型不能为空");
        }

        LambdaQueryWrapper<Merchant> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Merchant::getUserId, request.getUserId());
        Merchant one = merchantService.getOne(queryWrapper);
        if (one != null) {
            request.setMerId(one.getId().toString());
        }
        PageInfo<ProductFrontResponse> list = frontProductService.getHomepageList(request, pageParamRequest);
        List<ProductFrontResponse> list1 = list.getList();
        for (ProductFrontResponse productFrontResponse : list1) {
            ProductDetailResponse normal = frontProductService.getHomepageDetail(productFrontResponse.getId(), "normal");
            productFrontResponse.setProductDetailResponse(normal);
        }
        return CommonResult.success(CommonPage.restPage(list));
    }


}
