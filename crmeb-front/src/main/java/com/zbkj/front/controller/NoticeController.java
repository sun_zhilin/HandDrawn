package com.zbkj.front.controller;

import com.zbkj.common.model.user.User;
import com.zbkj.common.result.Result;
import com.zbkj.common.result.ResultUtils;
import com.zbkj.common.session.UserSession;
import com.zbkj.service.service.INoticeService;
import com.zbkj.service.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;

@Slf4j
@RestController
@RequestMapping("api/front/notice")
@Api(tags = "通知")
public class NoticeController {
    @Autowired
    private UserService userService;

    @Autowired
    private INoticeService iNoticeService;
    @GetMapping("/pullNotice")
    @ApiOperation(value = "拉取通知", notes = "拉取通知,将通过webscoket异步推送")
    public Result pullOfflineMessage(@RequestParam Long minId, HttpServletRequest request) {
        User info = userService.getInfo();
        UserSession userSession = new UserSession();
        userSession.setNickName(info.getNickname());
        userSession.setUserName(info.getAccount());
        userSession.setUserId(info.getId().longValue());
        userSession.setTerminal(0);
        request.setAttribute("session", userSession);
        iNoticeService.pullNotice(minId);
        return ResultUtils.success();
    }
}
