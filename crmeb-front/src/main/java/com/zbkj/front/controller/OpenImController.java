package com.zbkj.front.controller;


import com.zbkj.common.model.openim.ImOpenimUser;
import com.zbkj.service.service.ImOpenimUserService;
import com.zbkj.service.service.UserService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("api/front/openIm")
@Api(tags = "接入openIm")
public class OpenImController {
    @Autowired
    private UserService userService;

    @Autowired
    private ImOpenimUserService imOpenimUserService;
}
