package com.zbkj.front.controller;

import com.github.pagehelper.PageInfo;
import com.zbkj.common.model.novel.NovelAdv;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.*;
import com.zbkj.common.response.*;
import com.zbkj.common.result.CommonResult;
import com.zbkj.common.result.Result;
import com.zbkj.common.result.ResultUtils;
import com.zbkj.service.service.NovelAdvService;
import com.zbkj.service.service.NovelHistoryService;
import com.zbkj.service.service.NovelRollChapterService;
import com.zbkj.service.service.NovelService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/front/novel")
@Api(tags = "小说")
public class NovelController {

    @Autowired
    private NovelService novelService;

    @Autowired
    private NovelAdvService novelAdvService;

    @Autowired
    private NovelRollChapterService novelRollChapterService;



    @Autowired
    private NovelHistoryService novelHistoryService;
//    @RequestMapping(path = "/index",method = RequestMethod.GET)
//    public CommonResult<List<NovelListResponse>> index(){
//
//        List<NovelListResponse> list =  novelService.index();
//        return CommonResult.success(list);
//    }
    @RequestMapping(path = "/index",method = RequestMethod.GET)
    public CommonResult<PageInfo<NovelListResponse>> index(NovelListRequest listRequest){
        PageInfo<NovelListResponse> list =  novelService.index(listRequest);
        return CommonResult.success(list);
    }

    @RequestMapping(path = "/myList",method = RequestMethod.GET)
    public CommonResult<List<NovelListResponse>> myList(){
        try {
            return CommonResult.success(novelService.mylist());
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/myAuthor",method = RequestMethod.GET)
    public CommonResult<List<NovelListResponse>> myAuthor(){
        try {
            return CommonResult.success(novelService.myAuthor());
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/detail",method = RequestMethod.GET)
    public CommonResult<NovelDetailResponse> getDetail(@RequestParam("id") Integer id){

        return CommonResult.success(novelService.getDetail(id));
    }

    @RequestMapping(path = "/getAllChapter",method = RequestMethod.GET)
    public CommonResult<List<NovelAllChaptResponse>> getAllChapter(@RequestParam("id") Integer id){

        return CommonResult.success(novelService.getAllChapter(id));
    }

    @RequestMapping(path = "/getAllDraft",method = RequestMethod.GET)
    public CommonResult<List<NovelAllChaptResponse>> getAllDraft(@RequestParam("id") Integer id){

        return CommonResult.success(novelService.getAllDraft(id));
    }
    @RequestMapping(path = "/getGarbage",method = RequestMethod.GET)
    public CommonResult<List<NovelAllChaptResponse>> getGarbage(@RequestParam("id") Integer id){

        return CommonResult.success(novelService.getGarbage(id));
    }



    @RequestMapping(path = "/content",method = RequestMethod.GET)
    public CommonResult<NovelContentResponse> getContent(@RequestParam("chapterId") Integer id){
        try{
            return CommonResult.success(novelRollChapterService.getContent(id));
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/add",method = RequestMethod.POST)
    public CommonResult<Integer> addNovel(@RequestBody NovelAddRequest request){
        try {

            return CommonResult.success(novelService.add(request));
        }catch (Exception e) {
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/update",method = RequestMethod.POST)
    public CommonResult<String> update(@RequestBody NovelChapterUpdateRequest request){
        try{
            novelRollChapterService.chapterUpdate(request);
            return CommonResult.success();
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/release",method = RequestMethod.GET)
    public CommonResult<String> release(@RequestParam  Integer id){
        try{

            return CommonResult.success(novelRollChapterService.chapterOpen(id));
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/reduce",method = RequestMethod.GET)
    public CommonResult<String> reduce(@RequestParam  Integer id){
        try{

            return CommonResult.success(novelRollChapterService.reduce(id));
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/unrelease",method = RequestMethod.GET)
    public CommonResult<String> unrelease(@RequestParam Integer id){
        try{

            return CommonResult.success(novelRollChapterService.chapterunrelease(id));
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/delete",method = RequestMethod.GET)
    public CommonResult<String> delete(@RequestParam Integer id){
        try{

            return CommonResult.success(novelRollChapterService.delete(id));
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/addAuthority",method = RequestMethod.GET)
        public CommonResult<String> addAuthority(@RequestParam Integer uid,@RequestParam Integer novelId){
        try {
            return CommonResult.success(novelService.addAuthority(uid,novelId));
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/delAuthority",method = RequestMethod.GET)
    public CommonResult<String> delAuthority(@RequestParam Integer uid,@RequestParam Integer novelId){
        try {
            return CommonResult.success(novelService.delAuthority(uid,novelId));
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/showAuthority",method = RequestMethod.GET)
    public CommonResult<List<AuthorityListResponse>> showAuthority(@RequestParam Integer novelId){
        try {
            return CommonResult.success(novelService.showAuthority(novelId));
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/history",method = RequestMethod.POST)
    public CommonResult<String> history(@RequestBody NovelHistoryRequest request){
        try {
            return CommonResult.success(novelHistoryService.history(request));
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/historyDetail",method = RequestMethod.GET)
    public CommonResult<NovelHistoryResponse> history(@RequestParam Integer bookId) {
        try{
            return CommonResult.success(novelHistoryService.getDetail(bookId));
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/find",method = RequestMethod.GET)
    public CommonResult<List<NovelListResponse>> history(@RequestParam String articlename) {

        return CommonResult.success(novelService.findByName(articlename));
    }

    /**
     * 模糊查询小说
     *
     * @param request
     * @param keyword
     * @return
     */
    @RequestMapping(path = "/findByKeyword",method = RequestMethod.GET)
    public CommonResult<PageInfo<NovelListResponse>> findByKeyword(PageParamRequest request, @RequestParam String keyword) {

        return CommonResult.success(novelService.findByKeyword(request,keyword));
    }


    @RequestMapping(path="/getAdvertising",method = RequestMethod.GET)
    public CommonPage<NovelAdv> getAdvertising(Integer isRandom, Integer type, PageParamRequest request){
        if(isRandom==1){
            return CommonPage.restPage(novelAdvService.getAdvRandom(request));
        }else
            return CommonPage.restPage(novelAdvService.getAdvType(type,request));
    }

}
