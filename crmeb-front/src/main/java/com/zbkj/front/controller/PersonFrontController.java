package com.zbkj.front.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.zbkj.common.model.communityOrder.NoteTopic;
import com.zbkj.common.model.merchant.MerchantCategory;
import com.zbkj.common.model.together.TbJob;
import com.zbkj.common.model.user.UserTag;
import com.zbkj.common.result.CommonResult;
import com.zbkj.common.vo.ProCategoryCacheVo;
import com.zbkj.service.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/front/person")
@Api(tags = "人脉控制器") //配合swagger使用
public class PersonFrontController {
    @Autowired
    private ProductCategoryService productCategoryService;
    @Autowired
    private TbJobService tbJobService;

    @Autowired
    private MerchantCategoryService merchantCategoryService;

    @Autowired
    UserTagService userTagService;

    @Autowired
    private NoteTopicService noteTopicService;

    @ApiOperation(value = "查看角色列表")
    @RequestMapping(value = "/getTbJobList", method = RequestMethod.GET)
    public CommonResult<List<UserTag>> getJobList() {
        List<UserTag> list = userTagService.list();
        return CommonResult.success(list);
    }

    @ApiOperation(value = "分类缓存树")
    @RequestMapping(value = "/cache/tree", method = RequestMethod.GET)
    public CommonResult<List<ProCategoryCacheVo>> getMerchantCacheTree() {
        return CommonResult.success(productCategoryService.getMerchantCacheTree(null));
    }

    @ApiOperation(value = "查询人脉分类")
    @RequestMapping(value = "/getMerchantCategory", method = RequestMethod.GET)
    public CommonResult<List<MerchantCategory>> getMerchantCategory() {
        return CommonResult.success(merchantCategoryService.list());
    }

    @ApiOperation(value = "笔记主题列表")
    @RequestMapping(value = "/getNoteTopic", method = RequestMethod.GET)
    public CommonResult<List<NoteTopic>> getNoteTopic() {
        return CommonResult.success(noteTopicService.list());
    }

    @ApiOperation(value = "用户专题下拉框")
    @RequestMapping(value = "/getUserNoteTopic/{userId}", method = RequestMethod.GET)
    public CommonResult<List<NoteTopic>> getUserNoteTopic(@PathVariable Integer userId) {

        LambdaQueryWrapper<NoteTopic> lqw = Wrappers.lambdaQuery();
        lqw.eq(NoteTopic::getIsDel, false);
        lqw.eq(NoteTopic::getUid, userId);
        List<NoteTopic> list = noteTopicService.list(lqw);
        return CommonResult.success(list);
    }


}
