package com.zbkj.front.controller;

import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.CancelCollectRequest;
import com.zbkj.common.request.CancelNoteCollectRequest;
import com.zbkj.common.request.NoteCollectRequest;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.response.CommunityNoteCollectionRespone;
import com.zbkj.common.response.UserProductRelationResponse;
import com.zbkj.common.result.CommonResult;
import com.zbkj.service.service.EbUserNoteCollectionService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("api/front/collect")
@Api(tags = "用户 -笔记- 收藏")
public class CommunityNoteCollectionController {


    @Autowired
    private EbUserNoteCollectionService ebUserNoteCollectionService;
    @RequestMapping(value = "/add/note", method = RequestMethod.POST)
    public CommonResult<String> save(@RequestBody @Validated NoteCollectRequest request) {
        if (ebUserNoteCollectionService.add(request)) {
            return CommonResult.success();
        }
        return CommonResult.failed();
    }

    @ApiOperation(value = "取消收藏笔记")
    @RequestMapping(value = "/cancel/note", method = RequestMethod.POST)
    public CommonResult<String> delete(@RequestBody CancelNoteCollectRequest request) {
        if (ebUserNoteCollectionService.delete(request)) {
            return CommonResult.success();
        }
        return CommonResult.failed();
    }

    @ApiOperation(value = "收藏笔记展示")
    @RequestMapping(value = "/note/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<CommunityNoteCollectionRespone>> getList(@Validated PageParamRequest pageParamRequest) {
        return CommonResult.success(CommonPage.restPage(ebUserNoteCollectionService.getUserList(pageParamRequest)));
    }

}
