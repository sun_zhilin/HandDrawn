package com.zbkj.front.controller;


import com.zbkj.common.model.together.TogetherReply;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.TogetherReplyRequest;
import com.zbkj.common.response.CommunityCommentReplyResponse;
import com.zbkj.common.response.TogetherReplyResponse;
import com.zbkj.common.result.CommonResult;
import com.zbkj.service.service.TogetherReplyService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@Slf4j
@RestController
@RequestMapping("api/front/together")
@Api(tags = "共创评论控制器")
public class TogetherReplyController {

    @Autowired
    TogetherReplyService togetherReplyService;

    @ApiOperation(value = "评论")
    @RequestMapping(value = "/reply/add", method = RequestMethod.POST)
    public CommonResult<TogetherReplyResponse> add(@RequestBody TogetherReplyRequest request){
        try{
            return CommonResult.success(togetherReplyService.add(request));
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }

    }

    @ApiOperation(value = "评论删除")
    @RequestMapping(value = "/reply/delete/{replyId}", method = RequestMethod.POST)
    public CommonResult<Integer> deleteReply(@PathVariable("replyId") Integer replyId) {
        return CommonResult.success(togetherReplyService.deleteReply(replyId));
    }


    @ApiOperation(value = "共创评论列表")
    @RequestMapping(value = "/reply/list/{togetherId}", method = RequestMethod.GET)
    public CommonPage<TogetherReplyResponse> findNoteReplyPageList(@PathVariable("togetherId") Integer togetherId, PageParamRequest request) {
        return CommonPage.restPage(togetherReplyService.findNoteReplyPageList(togetherId, request));
    }

    @ApiOperation(value = "共创评论点赞/取消")
    @RequestMapping(value = "/reply/like/{replyId}", method = RequestMethod.POST)
    public CommonResult<Object> likeReply(@PathVariable("replyId") Integer replyId) {
        togetherReplyService.likeReply(replyId);
        return CommonResult.success();
    }
}
