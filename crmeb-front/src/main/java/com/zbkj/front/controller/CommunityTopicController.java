package com.zbkj.front.controller;

import com.zbkj.common.request.CommunityTopicSaveRequest;
import com.zbkj.common.result.CommonResult;
import com.zbkj.front.service.FrontCommunityTopicService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author lzhCreate
 * @create 2024-07-02 14:55
 */
@Slf4j
@RestController
@RequestMapping("api/front/community/topic")
@Api(tags = "社区话题用户模块")
public class CommunityTopicController {

    @Autowired
    private FrontCommunityTopicService communityTopicService;

    @ApiOperation(value = "用户添加社区话题")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public CommonResult<Object> add(@RequestBody @Validated CommunityTopicSaveRequest request) {
        communityTopicService.add(request);
        return CommonResult.success();
    }
}
