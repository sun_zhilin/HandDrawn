package com.zbkj.front.controller;


import com.zbkj.common.request.CustomPreorderRequest;
import com.zbkj.common.request.PreOrderRequest;
import com.zbkj.common.response.OrderNoResponse;
import com.zbkj.common.result.CommonResult;
import com.zbkj.front.service.FrontOrderService;
import com.zbkj.service.service.CustomService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("api/front/customOrder")
@Api(tags = "定制订单控制器")
public class CustomOrderController {
    @Autowired
    FrontOrderService orderService;

    @Autowired
    CustomService customService;

    @ApiOperation(value = "预下单")
    @RequestMapping(value = "/pre/order", method = RequestMethod.POST)
    public CommonResult<OrderNoResponse> preOrder(@RequestBody PreOrderRequest request) {
        return CommonResult.success(orderService.preOrder_V1_3(customService.preOrder(request)));
    }
}
