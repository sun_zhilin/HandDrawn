package com.zbkj.front.controller;

import com.zbkj.common.response.NovelCategoryResponse;
import com.zbkj.common.result.CommonResult;
import com.zbkj.service.service.NovelCategoryService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/front/novelCategory")
@Api(tags = "小说分类")
public class NovelCategoryController {

    @Autowired
    NovelCategoryService novelCategoryService;

    @RequestMapping(path = "/index",method = RequestMethod.GET)
    public CommonResult<List<NovelCategoryResponse>> index(){
            List<NovelCategoryResponse> index = novelCategoryService.index();

            return CommonResult.success(index);

    }
}
