package com.zbkj.front.controller;

import com.zbkj.common.model.Appeal;
import com.zbkj.common.result.CommonResult;
import com.zbkj.service.service.AppealService;
import com.zbkj.service.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("api/front/Appeal")
@Api(tags = "文章")
public class AppealContoller {

    @Autowired
    private AppealService appealService;

    @Autowired
    private UserService userService;
    @ApiOperation(value = "新申诉")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public CommonResult<Appeal> add(@RequestBody Appeal appeal){
        Integer userId = userService.getUserIdException();
        appeal.setUid(userId);
        appeal.setTargetUid(appeal.getUserId());
        appeal.setStatus(0);
        appealService.save(appeal);
        return CommonResult.success(appeal);
    }
}
