package com.zbkj.front.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.model.custom.Custom;
import com.zbkj.common.model.merchant.Merchant;
import com.zbkj.common.model.product.Product;
import com.zbkj.common.request.*;
import com.zbkj.common.response.*;
import com.zbkj.common.result.CommonResult;
import com.zbkj.front.service.FrontProductService;
import com.zbkj.service.service.*;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/front/custom")
@Api(tags = "定制控制器")
public class CustomController {

    @Autowired
    UserService userService;


    @Autowired
    CustomService customService;

    @Autowired
    CustomFeedbackService customFeedbackService;

    @Autowired
    CustomAttachService customAttachService;

    @Autowired
    MerchantService merchantService;
    @Autowired
    private FrontProductService productService;
    @RequestMapping(path = "/index",method = RequestMethod.GET)
    public CommonResult<CustomIndexResponse> index(@RequestParam Integer id){
        try {
            return CommonResult.success(customService.index(id));
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/join",method = RequestMethod.GET)
    public CommonResult<String> join(@RequestParam Integer id){
        try {
            return CommonResult.success(customService.join(id));
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/loadFeedback",method = RequestMethod.GET)
    public CommonResult<List<CustomFeedbackResponse>> loadFeedback(@RequestParam Integer id){
        try {
            return CommonResult.success(customFeedbackService.loadFeedback(id));
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }


    @RequestMapping(path = "/showFile",method = RequestMethod.GET)
    public CommonResult<List<CustomShowFileResponse>> showFile(@RequestParam Integer id,@RequestParam Integer type){
        try {
            return CommonResult.success(customAttachService.showFile(id,type));
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }
    @RequestMapping(path = "/uploadFile",method = RequestMethod.POST)
    public CommonResult<String> uploadFile(@RequestBody CustomAttachUpload request){
        try {
            CustomAttachUploadRequest customAttachUploadRequest = request.getParam();
            return CommonResult.success(customAttachService.uploadFile(customAttachUploadRequest));
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/feedback",method = RequestMethod.POST)
    public CommonResult<String> feedback(@RequestBody CustomFeedbackRequest request){
        try {
            return CommonResult.success(customFeedbackService.addFeedback(request));
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/add",method = RequestMethod.POST)
    public CommonResult<Integer> add(@RequestBody CustomAddRequest request){
        try {
            return CommonResult.success(customFeedbackService.add(request));
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/update",method = RequestMethod.POST)
    public CommonResult<String> update(@RequestBody CustomUpdateRequest request){
        try {
            return CommonResult.success(customService.updateStatue(request));
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    @RequestMapping(path = "/list",method = RequestMethod.GET)
    public CommonResult<List<CustomListResponse>>list(){
        try {
            return CommonResult.success(customService.getlist());
        }catch (Exception e){
            return CommonResult.failed(e.getMessage());
        }
    }

    //废弃
//    @RequestMapping(path = "/getCustom",method = RequestMethod.POST)
//    public CommonResult<PageInfo<ProductFrontResponse>> getCustom(@ModelAttribute @Validated ProductFrontSearchRequest request,
//                                                              @ModelAttribute @Validated PageParamRequest pageParamRequest
//    ){
//        try {
////            Integer userId = userService.getUserId();
//            Integer userId = 2;
//            LambdaQueryWrapper<Merchant> lqw = new LambdaQueryWrapper<>();
//            lqw.eq(Merchant::getUserId,userId);
//            Merchant merchant = merchantService.getOne(lqw);
//            request.setMerId(merchant.getId().toString());
//            request.setType(2);
//            request.setUserId(userId);
//            PageInfo<ProductFrontResponse> list = productService.getList(request, pageParamRequest);
//            return CommonResult.success(list);
//        }catch (Exception e){
//            return CommonResult.failed(e.getMessage());
//        }
//    }


}
