package com.zbkj.front.controller;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.zbkj.common.model.community.CommunityNotes;
import com.zbkj.common.model.travel.Travel;
import com.zbkj.common.model.travel.TravelApply;
import com.zbkj.common.model.travel.TravelTopic;
import com.zbkj.common.model.user.User;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.*;
import com.zbkj.common.response.CommunityNoteFrontPageResponse;
import com.zbkj.common.response.ProductFrontResponse;
import com.zbkj.common.response.TravelDetailResponse;
import com.zbkj.common.response.TravelListResponse;
import com.zbkj.common.result.CommonResult;
import com.zbkj.front.service.CommunityFrontService;
import com.zbkj.service.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.checkerframework.checker.units.qual.C;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/front/travel")
@Api(tags = "旅游控制器") //配合swagger使用
public class TravelController {

    @Autowired
    TravelService travelService;

    @Autowired
    CommunityFrontService communityFrontService;

    @Autowired
    TravelTopicService travelTopicService;

    @Autowired
    UserService userService;

    @Autowired
    TravelApplyService travelApplyService;

    @Autowired
    CommunityNotesService communityNotesService;


    @RequestMapping(path = "/index",method = RequestMethod.GET)
    public CommonResult<List<TravelListResponse>> index(){
        try {
            return CommonResult.success(travelService.index());
        }catch (Exception e){
            return  CommonResult.failed();
        }
    }

    @RequestMapping(path = "/shop",method = RequestMethod.POST)
    public CommonResult<CommonPage<ProductFrontResponse>> shop(@RequestBody TravelShopRequest request) {
        try {
            return CommonResult.success(CommonPage.restPage(travelService.shop(request)));
        }catch (Exception e){
            return  CommonResult.failed();
        }
    }

    @RequestMapping(path = "/detail",method = RequestMethod.POST)
    public CommonResult<TravelDetailResponse> detail(@RequestBody TravelDetailRequest request){
        try {
            TravelTopic travelTopic = travelTopicService.getById(request.getTravelId());
            CommunityNoteFrontDiscoverRequest communityNoteFrontDiscoverRequest = new CommunityNoteFrontDiscoverRequest();
            communityNoteFrontDiscoverRequest.setShowVideo(1);
            communityNoteFrontDiscoverRequest.setTopicId(travelTopic.getTopicId());
            communityNoteFrontDiscoverRequest.setPage(request.getPage());
            communityNoteFrontDiscoverRequest.setLimit(request.getLimit());

            TravelDetailResponse response =new TravelDetailResponse();
            response.setTravelId(request.getTravelId());
            response.setVideoList( CommonPage.restPage(communityFrontService.findDiscoverNoteList(communityNoteFrontDiscoverRequest)));
            Travel travel = travelService.getById(request.getTravelId());
            response.setContentImage(travel.getContentImage());
            return CommonResult.success(response);
        }catch (Exception e){
            return  CommonResult.failed();
        }
    }

    @ApiOperation(value = "添加或修改旅游申请")
    @RequestMapping(path = "/applyInfoCollect",method = RequestMethod.POST)
    public CommonResult<String> applyInfoCollect(@RequestBody TravelApply request) {
////        Integer userId = userService.getUserIdException();
//        //查询用户信息
//        User user = userService.getInfo();
//        //封装TravelApply实体类
//        request.setCreateBy(user.getId());
//        request.setTel(user.getPhone());
//        LambdaQueryWrapper<TravelApply> lqw = new LambdaQueryWrapper<>();
//        lqw.eq(TravelApply::getCreateBy, user.getId());
//        //根据createBy查询TravelApply
//        TravelApply travelApply = travelApplyService.getOne(lqw);
//        //创建用户id已存在就修改，否则添加。
//        if (travelApply != null) {
//            travelApplyService.update(request, lqw);
//            return CommonResult.success("提交成功");
//        }
//        travelApplyService.save(request);
//        return CommonResult.success("提交成功");
        // 获取当前用户信息
        User user = userService.getInfo();

        // 封装TravelApply实体类
        request.setCreateBy(user.getId());
        request.setTel(user.getPhone());

        // 使用LambdaQueryWrapper查询是否已有记录
        LambdaQueryWrapper<TravelApply> lqw = new LambdaQueryWrapper<>();
        lqw.eq(TravelApply::getCreateBy, user.getId());

        try {
            // 查询TravelApply是否存在
            TravelApply travelApply = travelApplyService.getOne(lqw);
            if (travelApply != null) {
                request.setId(travelApply.getId()); // 设置ID以便于更新
            }
            travelApplyService.saveOrUpdate(request);
            return CommonResult.success("提交成功");
        } catch (Exception e) {
            // 记录日志并返回错误信息
            e.printStackTrace();
            return CommonResult.failed("提交失败: " + e.getMessage());
        }
    }

    @RequestMapping(path = "/MyTravelNotes",method = RequestMethod.GET)
    public CommonPage<CommunityNotes> MyTravelNotes(PageParamRequest request){
        Integer userId=  userService.getUserIdException();
        LambdaQueryWrapper<CommunityNotes> lqw = new LambdaQueryWrapper<>();
        lqw.eq(CommunityNotes::getUid,userId);
        lqw.eq(CommunityNotes::getCategoryId,2);
        lqw.eq(CommunityNotes::getIsDel,0);
        Page page = PageHelper.startPage(request.getPage(),request.getLimit());
        List<CommunityNotes> notesList = communityNotesService.list(lqw);

        notesList.forEach(e->{
            LambdaQueryWrapper<TravelApply> travelApplyLambdaQueryWrapper = new LambdaQueryWrapper<>();
            travelApplyLambdaQueryWrapper.eq(TravelApply::getNoteId,e.getId());
            travelApplyLambdaQueryWrapper.eq(TravelApply::getIsShow,1);
            e.setApplyNum(travelApplyService.count(travelApplyLambdaQueryWrapper));
        });
        return CommonPage.restPage(CommonPage.copyPageInfo(page,notesList));
    }

    @ApiOperation(value = "获取旅游申请信息")
    @RequestMapping(path = "/getApplyInfo",method = RequestMethod.GET)
    public CommonPage<TravelApply> getApplyInfo(Integer noteId,PageParamRequest request) {
        LambdaQueryWrapper<TravelApply> lqw = new LambdaQueryWrapper<>();
        lqw.eq(TravelApply::getNoteId,noteId);
        lqw.eq(TravelApply::getIsShow,1);
        Page page = PageHelper.startPage(request.getPage(),request.getLimit());
        List<TravelApply> list = travelApplyService.list(lqw);

        list.forEach(e->{
            e.setUser(userService.getById(e.getCreateBy()));
        });
        return CommonPage.restPage(CommonPage.copyPageInfo(page,list));
    }

}
