package com.zbkj.front.controller;

import com.zbkj.common.result.CommonResult;
import com.zbkj.common.vo.FileResultVo;
import com.zbkj.service.service.EbMaterialService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/front/material")
@Api(tags = "素材附件接口")
public class MaterialController {

    @Autowired
    private EbMaterialService ebMaterialService;

    @GetMapping("/{preOrderNo}")
    public CommonResult<List<FileResultVo>> download(@PathVariable String preOrderNo){
        return CommonResult.success(ebMaterialService.download(preOrderNo));
    }
}
