package com.zbkj.front.controller;

import com.zbkj.common.model.user.User;
import com.zbkj.common.result.Result;
import com.zbkj.common.result.ResultUtils;
import com.zbkj.common.session.UserSession;
import com.zbkj.common.vo.GroupMessageVO;
import com.zbkj.service.dao.GroupMessageDTO;
import com.zbkj.service.service.IFriendService;
import com.zbkj.service.service.IGroupMessageService;
import com.zbkj.service.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import java.util.List;

@Api(tags = "群聊消息")
@RestController
@RequestMapping("api/front/message/group")
@RequiredArgsConstructor
public class GroupMessageController {

    private final IGroupMessageService groupMessageService;

    @Autowired
    private UserService userService;
    @Autowired
    private IFriendService friendService;

    @PostMapping("/send")
    @ApiOperation(value = "发送群聊消息", notes = "发送群聊消息")
    public Result<Long> sendMessage(@Valid @RequestBody GroupMessageDTO vo,HttpServletRequest request) {
        User info = userService.getInfo();
        UserSession userSession = new UserSession();
        userSession.setNickName(info.getNickname());
        userSession.setUserName(info.getAccount());
        userSession.setUserId(info.getId().longValue());
        userSession.setTerminal(0);
        request.setAttribute("session", userSession);
        return ResultUtils.success(groupMessageService.sendMessage(vo));
    }

    @DeleteMapping("/recall/{id}")
    @ApiOperation(value = "撤回消息", notes = "撤回群聊消息")
    public Result<Long> recallMessage(@NotNull(message = "消息id不能为空") @PathVariable Long id,HttpServletRequest request) {
        User info = userService.getInfo();
        UserSession userSession = new UserSession();
        userSession.setNickName(info.getNickname());
        userSession.setUserName(info.getAccount());
        userSession.setUserId(info.getId().longValue());
        userSession.setTerminal(0);
        request.setAttribute("session", userSession);
        groupMessageService.recallMessage(id);
        return ResultUtils.success();
    }


    @GetMapping("/loadMessage")
    @ApiOperation(value = "拉取消息", notes = "拉取消息,一次最多拉取100条")
    public Result<List<GroupMessageVO>> loadMessage(@RequestParam Long minId,HttpServletRequest request) {
        User info = userService.getInfo();
        UserSession userSession = new UserSession();
        userSession.setNickName(info.getNickname());
        userSession.setUserName(info.getAccount());
        userSession.setUserId(info.getId().longValue());
        userSession.setTerminal(0);
        request.setAttribute("session", userSession);
        return ResultUtils.success(groupMessageService.loadMessage(minId));
    }

    @GetMapping("/pullOfflineMessage")
    @ApiOperation(value = "拉取离线消息", notes = "拉取离线消息,消息将通过webscoket异步推送")
    public Result pullOfflineMessage(@RequestParam Long minId, HttpServletRequest request) {

        User info = userService.getInfo();
        UserSession userSession = new UserSession();
        userSession.setNickName(info.getNickname());
        userSession.setUserName(info.getAccount());
        userSession.setUserId(info.getId().longValue());
        userSession.setTerminal(0);
        request.setAttribute("session", userSession);
        groupMessageService.pullOfflineMessage(minId);
        return ResultUtils.success();
    }

    @PutMapping("/readed")
    @ApiOperation(value = "消息已读", notes = "将群聊中的消息状态置为已读")
    public Result readedMessage(@RequestParam Long groupId,HttpServletRequest request) {
        User info = userService.getInfo();
        UserSession userSession = new UserSession();
        userSession.setNickName(info.getNickname());
        userSession.setUserName(info.getAccount());
        userSession.setUserId(info.getId().longValue());
        userSession.setTerminal(0);
        request.setAttribute("session", userSession);
        groupMessageService.readedMessage(groupId);
        return ResultUtils.success();
    }

    @GetMapping("/findReadedUsers")
    @ApiOperation(value = "获取已读用户id", notes = "获取消息已读用户列表")
    public Result<List<Long>> findReadedUsers(@RequestParam Long groupId,@RequestParam Long messageId,HttpServletRequest request) {
        User info = userService.getInfo();
        UserSession userSession = new UserSession();
        userSession.setNickName(info.getNickname());
        userSession.setUserName(info.getAccount());
        userSession.setUserId(info.getId().longValue());
        userSession.setTerminal(0);
        request.setAttribute("session", userSession);
        groupMessageService.readedMessage(groupId);
        return ResultUtils.success(groupMessageService.findReadedUsers(groupId,messageId));
    }

    @GetMapping("/history")
    @ApiOperation(value = "查询聊天记录", notes = "查询聊天记录")
    public Result<List<GroupMessageVO>> recallMessage(@NotNull(message = "群聊id不能为空") @RequestParam Long groupId,
                                                      @NotNull(message = "页码不能为空") @RequestParam Long page,
                                                      @NotNull(message = "size不能为空") @RequestParam Long size,HttpServletRequest request) {
        User info = userService.getInfo();
        UserSession userSession = new UserSession();
        userSession.setNickName(info.getNickname());
        userSession.setUserName(info.getAccount());
        userSession.setUserId(info.getId().longValue());
        userSession.setTerminal(0);
        request.setAttribute("session", userSession);
        return ResultUtils.success(groupMessageService.findHistoryMessage(groupId, page, size));
    }
}

