package com.zbkj.admin.controller.platform;

import com.zbkj.common.model.communityOrder.CommunityOrder;
import com.zbkj.common.model.communityOrder.NoteTopic;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.merchant.MerchantSearchRequest;
import com.zbkj.common.result.CommonResult;
import com.zbkj.service.service.CommunityOrderService;
import com.zbkj.service.service.NoteTopicService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("api/admin/platform/note/communityOrder")
@Api(tags = "平台端笔记订单")
public class PlatformCommunityOrderController {

    @Autowired
    private CommunityOrderService communityOrderService;


    @ApiOperation(value = "笔记专题分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<CommunityOrder>> getList(@Validated PageParamRequest pageParamRequest, @Validated CommunityOrder communityOrder) {
        CommonPage<CommunityOrder> NoteTopicCommonPage = CommonPage.restPage(communityOrderService.getList(pageParamRequest,communityOrder));
        return CommonResult.success(NoteTopicCommonPage);
    }
}
