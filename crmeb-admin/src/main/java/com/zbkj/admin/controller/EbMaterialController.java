package com.zbkj.admin.controller;

import com.zbkj.common.page.CommonPage;
import com.zbkj.common.result.CommonResult;
import com.zbkj.service.entity.EbMaterial;
import com.zbkj.service.service.EbMaterialService;
import io.swagger.annotations.Api;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

/**
 * 素材附件(EbMaterial)表控制层
 *
 * @author makejava
 * @since 2024-03-19 09:26:48
 */
@Slf4j
@Api(tags = "附件控制器")
@RestController
@RequestMapping("ebMaterial")
public class EbMaterialController {
    /**
     * 服务对象
     */
    @Resource
    private EbMaterialService ebMaterialService;

    /**
     * 分页查询
     *
     * @param ebMaterial 筛选条件
     * @param pageRequest      分页对象
     * @return 查询结果
     */
    @GetMapping
    public CommonResult<CommonPage<EbMaterial>> queryByPage(EbMaterial ebMaterial, PageRequest pageRequest) {
        return CommonResult.success(CommonPage.restPage(ebMaterialService.queryByPage(ebMaterial, pageRequest)));
    }

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("/info/{id}")
    public CommonResult<EbMaterial> queryById(@PathVariable("id") Integer id) {
        return CommonResult.success(ebMaterialService.queryById(id));
    }

    /**
     * 新增数据
     *
     * @param ebMaterial 实体
     * @return 新增结果
     */
    @PostMapping
    public ResponseEntity<EbMaterial> add(EbMaterial ebMaterial) {
        return ResponseEntity.ok(this.ebMaterialService.insert(ebMaterial));
    }

    /**
     * 编辑数据
     *
     * @param ebMaterial 实体
     * @return 编辑结果
     */
    @PutMapping
    public ResponseEntity<EbMaterial> edit(EbMaterial ebMaterial) {
        return ResponseEntity.ok(this.ebMaterialService.update(ebMaterial));
    }

    /**
     * 删除数据
     *
     * @param id 主键
     * @return 删除是否成功
     */
    @DeleteMapping
    public ResponseEntity<Boolean> deleteById(Integer id) {
        return ResponseEntity.ok(this.ebMaterialService.deleteById(id));
    }


    @GetMapping("/download/{id}")
    public void download(HttpServletResponse response,@RequestParam Integer id) {

        EbMaterial ebMaterial = ebMaterialService.queryById(id);
        String url = ebMaterial.getUrl();



        response.reset();
        response.setContentType("application/octet-stream");
        response.setHeader("Content-disposition",
                "attachment;filename=" + ebMaterial.getName() + url.substring(url.lastIndexOf(".")));

        // 从文件读到servlet response输出流中
        File file = new File(url); // 改这里
        try (FileInputStream inputStream = new FileInputStream(file);) { // try-with-resources
            byte[] b = new byte[1024];
            int len;
            while ((len = inputStream.read(b)) > 0) {
                response.getOutputStream().write(b, 0, len);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

