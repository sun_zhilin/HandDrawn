package com.zbkj.admin.controller.platform;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.zbkj.common.exception.CrmebException;
import com.zbkj.common.model.community.CommunityNotes;
import com.zbkj.common.model.community.CommunityNotesProduct;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.*;
import com.zbkj.common.response.CommunityNoteDetailResponse;
import com.zbkj.common.response.CommunityNotePageDateResponse;
import com.zbkj.common.response.MerchantSearchResponse;
import com.zbkj.common.response.ProMerchantProductResponse;
import com.zbkj.common.result.CommonResult;
import com.zbkj.common.result.CommonResultCode;
import com.zbkj.common.utils.CrmebUtil;
import com.zbkj.service.service.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @ClassName CommunityNoteController
 * @Description 社区笔记控制器
 * @Author HZW
 * @Date 2023/3/7 11:32
 * @Version 1.0
 */
@Slf4j
@RestController
@RequestMapping("api/admin/platform/community/note")
@Api(tags = "社区笔记管理")
public class CommunityNoteController {



    @Autowired
    private CommunityNotesService notesService;

    @Autowired
    private SystemAttachmentService systemAttachmentService;

    @Autowired
    private CommunityNotesProductService notesProductService;

    @Autowired
    private TransactionTemplate transactionTemplate;

    @Autowired
    private UserService userService;

    @Autowired
    private OrderDetailService orderDetailService;

    @PreAuthorize("hasAuthority('platform:community:note:page:list')")
    @ApiOperation(value = "社区笔记分页列表")
    @RequestMapping(value = "/page/list", method = RequestMethod.GET)
    public CommonPage<CommunityNotePageDateResponse> findPageList(@Validated CommunityNoteSearchRequest request) {
        return CommonPage.restPage(notesService.findPageList(request));
    }

    @ApiOperation(value = "创建社区笔记")
    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public CommonResult<Object> createNote(@RequestBody @Validated CommunityNotes request) {
//        notesService.save(request);
//        Integer userId = userService.getUserIdException();
        List<String> images = request.getImages().stream().map(i -> {
            String s = systemAttachmentService.clearPrefix(i);

            return s;
        }).collect(Collectors.toList());
        String str = String.join(",", images);
        request.setImage(str);
        request.setCover(systemAttachmentService.clearPrefix(request.getCover()));
        if (ObjectUtil.isNotNull(request.getVideo()) && StrUtil.isNotBlank(request.getVideo())) {
            request.setVideo(systemAttachmentService.clearPrefix(request.getVideo()));
        }
        String[] split = request.getUserIds().split(",");
        List<CommunityNotesProduct> notesProductList = new ArrayList<>();
        Boolean execute = true;
        for (String s:split) {
            request.setUid(Integer.valueOf(s));
            if (StrUtil.isNotBlank(request.getProductIds())) {
                List<Integer> proIdList = CrmebUtil.stringToArray(request.getProductIds());
                proIdList.forEach(pid -> {
                    CommunityNotesProduct notesProduct = new CommunityNotesProduct();
                    notesProduct.setProductId(pid);
                    notesProduct.setIsPay(orderDetailService.isPurchased(pid, Integer.valueOf(s)) ? 1 : 0);
                    notesProductList.add(notesProduct);
                });
            }
            execute= transactionTemplate.execute(e -> {
                boolean save = notesService.save(request);
                if (!save) {
                    e.setRollbackOnly();
                    log.error("添加社区笔记失败，request = {}", request);
                    return Boolean.FALSE;
                }
                if (CollUtil.isNotEmpty(notesProductList)) {
                    notesProductList.forEach(p -> p.setNoteId(request.getId()));
                    notesProductService.saveBatch(notesProductList);
                }
                return Boolean.TRUE;
            });
        }
        if (!execute) {
            throw new CrmebException(CommonResultCode.ERROR.setMessage("新增笔记失败"));
        }
        return CommonResult.success();
    }

    @PreAuthorize("hasAuthority('platform:community:note:detail')")
    @ApiOperation(value = "社区笔记详情")
    @RequestMapping(value = "/detail/{id}", method = RequestMethod.GET)
    public CommunityNoteDetailResponse detail(@PathVariable("id") Integer id) {
        return notesService.detail(id);
    }

    @PreAuthorize("hasAuthority('platform:community:note:audit')")
    @ApiOperation(value = "社区笔记审核")
    @RequestMapping(value = "/audit", method = RequestMethod.POST)
    public CommonResult<Object> audit(@RequestBody @Validated CommonAuditRequest request) {
        notesService.audit(request);
        return CommonResult.success();
    }

    @PreAuthorize("hasAuthority('platform:community:note:forced:down')")
    @ApiOperation(value = "社区笔记强制下架")
    @RequestMapping(value = "/forced/down/{id}", method = RequestMethod.POST)
    public CommonResult<Object> forcedDown(@RequestBody @Validated CommonForcedDownRequest request) {
        notesService.forcedDown(request);
        return CommonResult.success();
    }

    @PreAuthorize("hasAuthority('platform:community:note:delete')")
    @ApiOperation(value = "社区笔记删除")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public CommonResult<Object> delete(@PathVariable("id") Integer id) {
        notesService.delete(id);
        return CommonResult.success();
    }

    @PreAuthorize("hasAuthority('platform:community:note:category:batch:update')")
    @ApiOperation(value = "社区笔记分类批量修改")
    @RequestMapping(value = "/category/batch/update", method = RequestMethod.POST)
    public CommonResult<Object> categoryBatchUpdate(@RequestBody @Validated CommunityNoteCategoryBatchUpdateRequest request) {
        notesService.categoryBatchUpdate(request);
        return CommonResult.success();
    }

    @PreAuthorize("hasAuthority('platform:community:note:star:update')")
    @ApiOperation(value = "社区笔记推荐星级编辑")
    @RequestMapping(value = "/star/update", method = RequestMethod.POST)
    public CommonResult<Object> updateStar(@RequestBody @Validated CommonStarUpdateRequest request) {
        notesService.updateStar(request);
        return CommonResult.success();
    }

    @PreAuthorize("hasAuthority('platform:community:note:repley:force:off:switch')")
    @ApiOperation(value = "社区笔记评论强制关闭开关")
    @RequestMapping(value = "/reply/force/off/{id}", method = RequestMethod.POST)
    public CommonResult<Object> replyForceOffSwitch(@PathVariable("id") Integer id) {
        notesService.replyForceOffSwitch(id);
        return CommonResult.success();
    }
}
