package com.zbkj.admin.controller.platform;


import com.zbkj.common.model.communityOrder.NoteTopic;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.NoteTopicRequest;
import com.zbkj.common.result.CommonResult;
import com.zbkj.common.utils.CrmebDateUtil;
import com.zbkj.service.service.NoteTopicService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.List;

@Slf4j
@RestController
@RequestMapping("api/admin/platform/note/noteTopic")
@Api(tags = "平台端笔记专题")
public class PlatformNoteTopicController {

    @Autowired
    private NoteTopicService noteTopicService;


    @ApiOperation(value = "笔记专题分页列表") //配合swagger使用
    @RequestMapping(value = "/list", method = RequestMethod.GET)
    public CommonResult<CommonPage<NoteTopic>> getList(@Validated PageParamRequest pageParamRequest) {
        CommonPage<NoteTopic> NoteTopicCommonPage = CommonPage.restPage(noteTopicService.getList(pageParamRequest));
        return CommonResult.success(NoteTopicCommonPage);
    }


    @ApiOperation(value = "新增笔记专题")
    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public CommonResult<String> save(@RequestBody @Validated NoteTopicRequest noteTopicRequest) {
        Date nowDate = CrmebDateUtil.nowDateTime();
        noteTopicRequest.setCreateTime(nowDate);
        if (noteTopicService.create(noteTopicRequest)) {
            return CommonResult.success();
        }
        return CommonResult.failed();
    }


    @ApiOperation(value = "删除笔记专题")
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.POST)
    public CommonResult<String> delete(@PathVariable(value = "id") Integer id) {
        if (noteTopicService.delete(id)) {
            return CommonResult.success();
        }
        return CommonResult.failed();
    }


    @ApiOperation(value = "修改笔记专题")
    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public CommonResult<String> update(@RequestBody @Validated NoteTopicRequest noteTopicRequest) {
        if (noteTopicService.updateTag(noteTopicRequest)) {
            return CommonResult.success();
        }
        return CommonResult.failed();
    }


    @ApiOperation(value = "笔记专题全部列表") //配合swagger使用
    @RequestMapping(value = "/all/list", method = RequestMethod.GET)
    public CommonResult<List<NoteTopic>> getAllList() {
        return CommonResult.success(noteTopicService.getAllList());
    }
}
