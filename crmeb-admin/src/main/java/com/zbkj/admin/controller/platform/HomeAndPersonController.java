package com.zbkj.admin.controller.platform;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.zbkj.common.model.merchant.Merchant;
import com.zbkj.common.model.merchant.MerchantCategory;
import com.zbkj.common.model.user.User;
import com.zbkj.common.page.CommonPage;
import com.zbkj.common.request.PageParamRequest;
import com.zbkj.common.request.ProductFrontSearchRequest;
import com.zbkj.common.response.ProductFrontResponse;
import com.zbkj.common.result.CommonResult;

import com.zbkj.service.service.MerchantCategoryService;
import com.zbkj.service.service.MerchantService;
import com.zbkj.service.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@Slf4j
@RestController
@RequestMapping("api/admin/platform/person")
@Api(tags = "人脉控制器") //配合swagger使用
public class HomeAndPersonController {

    @Autowired
    private UserService userService;

    @Autowired
    private MerchantService merchantService;


    @Autowired
    private MerchantCategoryService merchantCategoryService;


    @ApiOperation(value = "分类人脉列表")
    @RequestMapping(value = "/getI", method = RequestMethod.GET)
    public CommonResult< List<MerchantCategory>> getMerchantCategory() {
        List<MerchantCategory> list = merchantCategoryService.list();
        for (MerchantCategory category:list){
            Integer id = category.getId();
            LambdaQueryWrapper<Merchant> lqw = Wrappers.lambdaQuery();

            lqw.in(Merchant::getCategoryId, id);
            lqw.isNotNull(Merchant::getUserId);
            List<Merchant> list1 = merchantService.list(lqw);
            List<Integer> collect = list1.stream().map(Merchant::getUserId).collect(Collectors.toList());


            String ids = StringUtils.join(collect.toArray(), ",");
            LambdaQueryWrapper<User> queryWrapper = Wrappers.lambdaQuery();

            queryWrapper.in(User::getId, ids);
            List<User> userList = userService.list(queryWrapper);
            category.setUserList(userList);
        }
        return CommonResult.success(list);
    }

    @ApiOperation(value = "查询人脉分类")
    @RequestMapping(value = "/getMerchantCategoryList", method = RequestMethod.GET)
    public CommonResult<List<MerchantCategory>> getMerchantCategoryList() {
        return CommonResult.success(merchantCategoryService.list());
    }


    @ApiOperation(value = "普通人脉列表")
    @RequestMapping(value = "/getR", method = RequestMethod.GET)
    public CommonResult<List<User>> getR() {
        List<User> list = userService.list();
        for (User user:list){
            LambdaQueryWrapper<Merchant> queryWrapper = new LambdaQueryWrapper<>();
            queryWrapper.eq(Merchant::getUserId, user.getId());
            Merchant one = merchantService.getOne(queryWrapper);
            if (one!=null) {
                LambdaQueryWrapper<MerchantCategory> lambdaQueryWrapper = new LambdaQueryWrapper<>();
                lambdaQueryWrapper.in(MerchantCategory::getId, one.getCategoryId());
                List<String> collect = merchantCategoryService.list(lambdaQueryWrapper).stream().map(MerchantCategory::getName).collect(Collectors.toList());
                String ids = StringUtils.join(collect.toArray(), ",");
                user.setCategory(ids);
            }
        }
        return CommonResult.success(list);
    }


}
