package com.zbkj.admin.task.together;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.zbkj.admin.task.order.OrderPaySuccessTask;
import com.zbkj.common.model.together.TogetherJobExam;
import com.zbkj.common.utils.CrmebDateUtil;
import com.zbkj.service.service.TogetherJobExamService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.time.Instant;
import java.util.List;

@Component("ExamTask")
public class ExamTask {

    private static final Logger logger = LoggerFactory.getLogger(ExamTask.class);

    @Autowired
    private TogetherJobExamService togetherJobExamService;

    public void examOutTimeTask(){
        logger.info("---examOutTimeTask task------produce Data with fixed rate task: Execution Time - {}", CrmebDateUtil.nowDateTime());

        try {
            Instant now = Instant.now();
            LambdaQueryWrapper<TogetherJobExam> lqw = new LambdaQueryWrapper<>();
            lqw.le(TogetherJobExam::getStatus,1);
            List<TogetherJobExam> list = togetherJobExamService.list(lqw);
            list.forEach(item->{
                // 获取当前时间


                // 将 Date 转换为 Instant
                Instant expiration = item.getLimitTime().toInstant();

                // 比较当前时间和给定时间
                if (now.isAfter(expiration)) {
                    switch (item.getStatus()){
                        case 1:
                            item.setStatus(4);
                            break;
                        case 0:
                            item.setStatus(5);
                            break;
                    }
                    // 如果过期，执行特定方法
                    togetherJobExamService.updateById(item);
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
            logger.error("examOutTimeTask.task" + " | msg : " + e.getMessage());
        }
    }
}
