package com.zbkj.admin.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public  class UserNameGenerate {
    private static String[] FIRST_NAME_ARRAY ={
            "赵","钱","孙","李","周","吴","郑","王","冯","陈","褚","卫","蒋","沉","韩","杨","朱","秦","尤","许",
            "何","吕","施","张","孔","曹","严","华","金","魏","陶","姜","戚","谢","邹","喻","柏","水","窦","章","云","苏","潘","葛","奚","范","彭","郎",
            "鲁","韦","昌","马","苗","凤","花","方","俞","任","袁","柳","酆","鲍","史","唐","费","廉","岑","薛","雷","贺","倪","汤","滕","殷",
            "罗","毕","郝","邬","安","常","乐","于","时","傅","皮","卞","齐","康","伍","馀","元","卜","顾","孟","平","黄","和",
            "穆","萧","尹","姚","邵","湛","汪","祁","毛","禹","狄","米","贝","明","臧","计","伏","成","戴","谈","宋","茅","庞","熊","纪","舒",
            "屈","项","祝","董","梁","杜","阮","蓝","闵","席","季","麻","强","贾","路","娄","危","江","童","颜","郭","梅","盛","林","刁","钟",
            "徐","邱","骆","高","夏","蔡","田","樊","胡","凌","霍","虞","万","支","柯","昝","管","卢","莫","经","房","裘","缪","干","解","应",
            "宗","丁","宣","贲","邓","郁","单","杭","洪","包","诸","左","石","崔","吉","钮","龚","程","嵇","邢","滑","裴","陆","荣","翁","荀",
            "羊","于","惠","甄","曲","家","封","芮","羿","储","靳","汲","邴","糜","松","井","段","富","巫","乌","焦","巴","弓","牧","隗","山",
            "谷","车","侯","宓","蓬","全","郗","班","仰","秋","仲","伊","宫","宁","仇","栾","暴","甘","钭","厉","戎","祖","武","符","刘","景",
            "詹","束","龙","叶","幸","司","韶","郜","黎","蓟","溥","印","宿","白","怀","蒲","邰","从","鄂","索","咸","籍","赖","卓","蔺","屠",
            "蒙","池","乔","阴","鬱","胥","能","苍","双","闻","莘","党","翟","谭","贡","劳","逄","姬","申","扶","堵","冉","宰","郦","雍","却",
            "璩","桑","桂","濮","牛","寿","通","边","扈","燕","冀","浦","尚","农","温","别","庄","晏","柴","瞿","阎","充","慕","连","茹","习",
            "宦","艾","鱼","容","向","古","易","慎","戈","廖","庾","终","暨","居","衡","步","都","耿","满","弘","匡","国","文","寇","广","禄",
            "阙","东","欧","殳","沃","利","蔚","越","夔","隆","师","巩","厍","聂","晁","勾","敖","融","冷","訾","辛","阚","那","简","饶","空",
            "曾","毋","沙","乜","养","鞠","须","丰","巢","关","蒯","相","查","后","荆","红","游","郏","竺","权","逯","盖","益","桓","公","仉",
            "督","岳","帅","缑","亢","况","郈","有","琴","归","海","晋","楚","闫","法","汝","鄢","涂","钦","商","牟","佘","佴","伯","赏","墨",
            "哈","谯","篁","年","爱","阳","佟","言","福","南","火","铁","迟","漆","官","冼","真","展","繁","檀","祭","密","敬","揭","舜","楼",
            "疏","冒","浑","挚","胶","随","高","皋","原","种","练","弥","仓","眭","蹇","覃","阿","门","恽","来","綦","召","仪","风","介","巨",
            "木","京","狐","郇","虎","枚","抗","达","杞","苌","折","麦","庆","过","竹","端","鲜","皇","亓","老","是","秘","畅","邝","还","宾",
            "闾","辜","纵","侴","万俟","司马","上官","欧阳","夏侯","诸葛","闻人","东方","赫连","皇甫","羊舌","尉迟","公羊","澹台","公冶","宗正",
            "濮阳","淳于","单于","太叔","申屠","公孙","仲孙","轩辕","令狐","钟离","宇文","长孙","慕容","鲜于","闾丘","司徒","司空","兀官","司寇",
            "南门","呼延","子车","颛孙","端木","巫马","公西","漆凋","车正","壤驷","公良","拓跋","夹谷","宰父","谷梁","段干","百里","东郭","微生",
            "梁丘","左丘","东门","西门","南宫","第五","公仪","公乘","太史","仲长","叔孙","屈突","尔朱","东乡","相裡","胡母","司城","张廖","雍门",
            "毋丘","贺兰","綦毋","屋庐","独孤","南郭","北宫","王孙","羽", "芳","月", "若", "叱吒","魔","幽","吕","仙"};

    private static String[] LAST_NAME_ARRAY = { "伟", "刚", "勇", "毅", "俊", "峰", "强", "军", "平", "保", "东", "文", "辉", "力", "明", "永", "健", "世", "广", "志", "义", "兴", "良", "海", "山", "仁", "波", "宁", "贵", "福", "生", "龙", "元", "全", "国", "胜", "学", "祥", "才", "发", "武", "新", "利", "清", "飞", "彬", "富", "顺", "信", "子", "杰", "涛", "昌", "成", "康", "星", "光", "天", "达", "安", "岩", "中", "茂", "进", "林", "有", "坚", "和", "彪", "博", "诚", "先", "敬", "震", "振", "壮", "会", "思", "群", "豪", "心", "邦", "承", "乐", "绍", "功", "松", "善", "厚", "庆", "磊", "民", "友", "裕", "河", "哲", "江", "超", "浩", "亮", "政", "谦", "亨", "奇", "固", "之", "轮", "翰", "朗", "伯", "宏", "言", "若", "鸣", "朋", "斌", "梁", "栋", "维", "启", "克", "伦", "翔", "旭", "鹏", "泽", "晨", "辰", "士", "以", "建", "家", "致", "树", "炎", "德", "行", "时", "泰", "盛", "雄", "琛", "钧", "冠", "策", "腾", "楠", "榕", "风", "航", "弘", "秀", "娟", "英", "华", "慧", "巧", "美", "娜", "静", "淑", "惠", "珠", "翠", "雅", "芝", "玉", "萍", "红", "娥", "玲", "芬", "芳", "燕", "彩", "春", "菊", "兰", "凤", "洁", "梅", "琳", "素", "云", "莲", "真", "环", "雪", "荣", "爱", "妹", "霞", "香", "月", "莺", "媛", "豔", "瑞", "凡", "佳", "嘉", "琼", "勤", "珍", "贞", "莉", "桂", "娣", "叶", "璧", "璐", "娅", "琦", "晶", "妍", "茜", "秋", "珊", "莎", "锦", "黛", "青", "倩", "婷", "姣", "婉", "娴", "瑾", "颖", "露", "瑶", "怡", "婵", "雁", "蓓", "纨", "仪", "荷", "丹", "蓉", "眉", "君", "琴", "蕊", "薇", "菁", "梦", "岚", "苑", "婕", "馨", "瑗", "琰", "韵", "融", "园", "艺", "咏", "卿", "聪", "澜", "纯", "毓", "悦", "昭", "冰", "爽", "琬", "茗", "羽", "希", "欣", "飘", "育", "滢", "馥", "筠", "柔", "竹", "霭", "凝", "晓", "欢", "霄", "枫", "芸", "菲", "寒", "伊", "亚", "宜", "可", "姬", "舒", "影", "荔", "枝", "丽", "阳", "妮", "宝", "贝", "初", "程", "梵", "罡", "恒", "鸿", "桦", "骅", "剑", "娇", "纪", "宽", "苛", "灵", "玛", "媚", "琪", "晴", "容", "睿", "烁", "堂", "唯", "威", "韦", "雯", "苇", "萱", "阅", "彦", "宇", "雨", "洋", "忠", "宗", "曼", "紫", "逸", "贤", "蝶", "菡", "绿", "蓝", "儿", "翠", "烟", "小", "惜", "霸", "主", "郡", "魔", "幽", "多", "仙" };

    private static final String[] headerNameArray = new String[]{"快乐的",
            "冷静的",
            "醉熏的",
            "潇洒的",
            "糊涂的",
            "积极的",
            "冷酷的",
            "深情的",
            "粗暴的",
            "温柔的",
            "可爱的",
            "愉快的",
            "义气的",
            "认真的",
            "威武的",
            "帅气的",
            "传统的",
            "潇洒的",
            "漂亮的",
            "自然的",
            "专一的",
            "听话的",
            "昏睡的",
            "狂野的",
            "等待的",
            "搞怪的",
            "幽默的",
            "魁梧的",
            "活泼的",
            "开心的",
            "高兴的",
            "超帅的",
            "留胡子的",
            "坦率的",
            "直率的",
            "轻松的",
            "痴情的",
            "完美的",
            "精明的",
            "无聊的",
            "有魅力的",
            "丰富的",
            "繁荣的",
            "饱满的",
            "炙热的",
            "暴躁的",
            "碧蓝的",
            "俊逸的",
            "英勇的",
            "健忘的",
            "故意的",
            "无心的",
            "土豪的",
            "朴实的",
            "兴奋的",
            "幸福的",
            "淡定的",
            "不安的",
            "阔达的",
            "孤独的",
            "独特的",
            "疯狂的",
            "时尚的",
            "落后的",
            "风趣的",
            "忧伤的",
            "大胆的",
            "爱笑的",
            "矮小的",
            "健康的",
            "合适的",
            "玩命的",
            "沉默的",
            "斯文的",
            "香蕉",
            "苹果",
            "鲤鱼",
            "鳗鱼",
            "任性的",
            "细心的",
            "粗心的",
            "大意的",
            "甜甜的",
            "酷酷的",
            "健壮的",
            "英俊的",
            "霸气的",
            "阳光的",
            "默默的",
            "大力的",
            "孝顺的",
            "忧虑的",
            "着急的",
            "紧张的",
            "善良的",
            "凶狠的",
            "害怕的",
            "重要的",
            "危机的",
            "欢喜的",
            "欣慰的",
            "满意的",
            "跳跃的",
            "诚心的",
            "称心的",
            "如意的",
            "怡然的",
            "娇气的",
            "无奈的",
            "无语的",
            "激动的",
            "愤怒的",
            "美好的",
            "感动的",
            "激情的",
            "激昂的",
            "震动的",
            "虚拟的",
            "超级的",
            "寒冷的",
            "明理的",
            "犹豫的",
            "忧郁的",
            "寂寞的",
            "奋斗的",
            "勤奋的",
            "现代的",
            "过时的",
            "稳重的",
            "热情的",
            "含蓄的",
            "无辜的",
            "多情的",
            "纯真的",
            "拉长的",
            "热心的",
            "从容的",
            "体贴的",
            "风中的",
            "曾经的",
            "追寻的",
            "儒雅的",
            "优雅的",
            "开朗的",
            "外向的",
            "内向的",
            "清爽的",
            "文艺的",
            "长情的",
            "平常的",
            "单身的",
            "伶俐的",
            "高大的",
            "懦弱的",
            "柔弱的",
            "爱笑的",
            "乐观的",
            "耍酷的",
            "酷炫的",
            "神勇的",
            "年轻的",
            "唠叨的",
            "瘦瘦的",
            "无情的",
            "包容的",
            "顺心的",
            "畅快的",
            "舒适的",
            "靓丽的",
            "负责的",
            "背后的",
            "简单的",
            "谦让的",
            "彩色的",
            "缥缈的",
            "欢呼的",
            "生动的",
            "复杂的",
            "慈祥的",
            "仁爱的",
            "魔幻的",
            "虚幻的",
            "淡然的",
            "受伤的",
            "雪白的",
            "高高的",
            "糟糕的",
            "顺利的",
            "闪闪的",
            "羞涩的",
            "缓慢的",
            "迅速的",
            "优秀的",
            "聪明的",
            "含糊的",
            "俏皮的",
            "淡淡的",
            "坚强的",
            "平淡的",
            "欣喜的",
            "能干的",
            "灵巧的",
            "友好的",
            "机智的",
            "机灵的",
            "正直的",
            "谨慎的",
            "俭朴的",
            "殷勤的",
            "虚心的",
            "辛勤的",
            "自觉的",
            "无私的",
            "无限的",
            "踏实的",
            "老实的",
            "现实的",
            "可靠的",
            "务实的",
            "拼搏的",
            "个性的",
            "粗犷的",
            "活力的",
            "成就的",
            "勤劳的",
            "单纯的",
            "落寞的",
            "朴素的",
            "悲凉的",
            "忧心的",
            "洁净的",
            "清秀的",
            "自由的",
            "小巧的",
            "单薄的",
            "贪玩的",
            "刻苦的",
            "干净的",
            "壮观的",
            "和谐的",
            "文静的",
            "调皮的",
            "害羞的",
            "安详的",
            "自信的",
            "端庄的",
            "坚定的",
            "美满的",
            "舒心的",
            "温暖的",
            "专注的",
            "勤恳的",
            "美丽的",
            "腼腆的",
            "优美的",
            "甜美的",
            "甜蜜的",
            "整齐的",
            "动人的",
            "典雅的",
            "尊敬的",
            "舒服的",
            "妩媚的",
            "秀丽的",
            "喜悦的",
            "甜美的",
            "彪壮的",
            "强健的",
            "大方的",
            "俊秀的",
            "聪慧的",
            "迷人的",
            "陶醉的",
            "悦耳的",
            "动听的",
            "明亮的",
            "结实的",
            "魁梧的",
            "标致的",
            "清脆的",
            "敏感的",
            "光亮的",
            "大气的",
            "老迟到的",
            "知性的",
            "冷傲的",
            "呆萌的",
            "野性的",
            "隐形的",
            "笑点低的",
            "微笑的",
            "笨笨的",
            "难过的",
            "沉静的",
            "火星上的",
            "失眠的",
            "安静的",
            "纯情的",
            "要减肥的",
            "迷路的",
            "烂漫的",
            "哭泣的",
            "贤惠的",
            "苗条的",
            "温婉的",
            "发嗲的",
            "会撒娇的",
            "贪玩的",
            "执着的",
            "眯眯眼的",
            "花痴的",
            "想人陪的",
            "眼睛大的",
            "高贵的",
            "傲娇的",
            "心灵美的",
            "爱撒娇的",
            "细腻的",
            "天真的",
            "怕黑的",
            "感性的",
            "飘逸的",
            "怕孤独的",
            "忐忑的",
            "高挑的",
            "傻傻的",
            "冷艳的",
            "爱听歌的",
            "还单身的",
            "怕孤单的",
            "懵懂的"};



    public static String getUserName(){

        //随机数生成器
        Random random = new Random();

        String name = headerNameArray[random.nextInt(headerNameArray.length)] +FIRST_NAME_ARRAY[random.nextInt(FIRST_NAME_ARRAY.length)] + LAST_NAME_ARRAY[random.nextInt(LAST_NAME_ARRAY.length)];
        return name ;
    }




    public static void main(String[] args) {
        for (int i=0;i<100;i++) {
            System.out.println(getUserName());
        }

    }



}
